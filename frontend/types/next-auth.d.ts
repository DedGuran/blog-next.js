// nextauth.d.ts
import { DefaultSession, DefaultUser } from 'next-auth'
import { ResponseUser } from '../utils/api/types'
// Define a role enum
export enum Role {
  user = 'user',
  admin = 'admin',
}
// common interface for JWT and Session
interface IUser extends DefaultUser {
  role?: Role
}
declare module 'next-auth' {
  interface Session {
    user: ResponseUser
  }
}
declare module 'next-auth/jwt' {
  interface JWT extends IUser {}
}
