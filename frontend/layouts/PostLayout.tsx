// PostLayout.js
import React from 'react'
import Head from 'next/head'

const PostLayout = ({ title, description, children }) => {
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta charSet="utf-8" />
      </Head>
      {children}
    </div>
  )
}

export default PostLayout
