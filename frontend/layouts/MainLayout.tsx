import React from 'react'

interface MainLayoutProps {
  hideComments?: boolean
  hideMenu?: boolean
  contentFullWidth?: boolean
  className?: string
}

export const MainLayout: React.FC<MainLayoutProps> = ({ children }) => {
  return <div className="xl:container xl:mx-auto ">{children}</div>
}
