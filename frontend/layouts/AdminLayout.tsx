// PostLayout.js
import React from 'react'
import Head from 'next/head'
import { AdminSidebar } from '../components/AdminSidebar'

const AdminLayout = ({ title, description, children }) => {
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta charSet="utf-8" />
      </Head>
      <AdminSidebar>
        <main className="bg-gray-100 min-h-screen">{children}</main>
      </AdminSidebar>
    </div>
  )
}

export default AdminLayout
