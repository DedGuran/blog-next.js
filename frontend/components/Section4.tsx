import React, { useEffect, useState } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { PopularPost } from './_childComponent/PopularPost'
import { CategoryPost } from './_childComponent/CategoryPost'
import Link from 'next/link'
export default function Section4(posts) {
  const [categoryData, setCategoryData] = useState([])
  const seporateArr = (data) => {
    return data.reduce((acc, curr) => {
      if (!acc[curr.category.category]) {
        acc[curr.category.category] = []
      }
      acc[curr.category.category].push(curr)
      return acc
    }, {})
  }

  useEffect(() => {
    setCategoryData(seporateArr(posts.postsData))
  }, [posts])

  return (
    <section className="contant mx-auto md:px-20 py-2">
      <Swiper
        className="xl:container"
        slidesPerView={2}
        autoplay={{ delay: 5000 }}
      >
        <div className="grid lg:grid-cols-2">
          {Object.entries(categoryData)?.map(([category, posts]) => (
            <SwiperSlide key={posts.id}>
              <div className="item">
                <h1 className="font-bold text-4xl py-12">{category}</h1>
                <div className="flex flex-col gap-6">
                  {posts?.map((obj) => (
                    <CategoryPost
                      key={obj.id}
                      id={obj.id}
                      image={
                        obj.body.find((obj) => obj.type === 'image')?.data?.file
                          .url
                      }
                      category={obj.category.category}
                      categoryId={obj.category.id}
                      date={obj.createdAt}
                      description={obj.subtitle}
                      title={obj.title}
                      userName={obj.user.fullName}
                      userFoto={obj.user.avatar}
                      userRole={obj.user.role}
                      userStatus={obj.user.status}
                    />
                  ))}
                </div>
              </div>
            </SwiperSlide>
          ))}
        </div>
      </Swiper>
    </section>
  )
}
