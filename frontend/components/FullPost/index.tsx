import React from 'react'
import { OutputBlockData, OutputData } from '@editorjs/editorjs'
import { useComments } from '../../hooks/useComments'
import AuthorSection from '../_childComponent/AuthorSection'
import { Related } from '../_childComponent/Ralated'

interface FullPostProps {
  title: string
  blocks: OutputData['blocks']
  views: number
  postId: number
  subtitle: string
}

export const FullPost: React.FC<FullPostProps> = ({
  title,
  blocks,
  views,
  postId,
  subtitle,
}) => {
  const { comments, setComments } = useComments(postId)
  return (
    <section className="container mx-auto md:px-2 py-10 w-1/2">
      {/* <div className="flex justify-center">
                <AuthorSection/>
            </div> */}
      <div className="post">
        <h1 className="font-bold text-4xl text-center pb-5">{title}</h1>
        <p className="text-gray-500 text-xl text-center">{subtitle}</p>
        {blocks.map((obj: OutputBlockData) => {
          if (obj.type == 'paragraph') {
            return (
              <div
                className="text-gray-600 text-lg flex flex-col gap-4"
                key={obj.id}
              >
                <p>{obj.data.text}</p>
              </div>
            )
          } else if (obj.type == 'list') {
            return (
              <ul>
                {obj.data.items.map((el: 'string', i: 'string') => {
                  return (
                    <li>
                      {i + 1}.{el}
                    </li>
                  )
                })}
              </ul>
            )
          } else if (obj.type == 'image') {
            return (
              <div className="py-10">
                <figure>
                  <img
                    width="900"
                    height="600"
                    src={obj.data.file.url}
                    alt=""
                  />
                  <figcaption className="figure-caption">
                    {obj.data.file.caption}
                  </figcaption>
                </figure>
              </div>
            )
          }
        })}
      </div>
    </section>
  )
}
