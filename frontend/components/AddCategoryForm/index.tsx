import React, { useState } from 'react'
import styles from './AddCategoryForm.module.scss'
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  FormControl,
  Input,
  InputLabel,
  Select,
} from '@material-ui/core'
import { SelectChangeEvent } from '@mui/material'
import { Api } from '../../utils/api'
import router from 'next/router'
import AddIcon from '@mui/icons-material/Add'

interface AddCategoryFormProps {}

export const AddCategoryForm: React.FC<AddCategoryFormProps> = ({}) => {
  const [open, setOpen] = useState(false)
  const [category, setCategory] = useState('')
  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = (
    event: React.SyntheticEvent<unknown>,
    reason?: string
  ) => {
    if (reason !== 'backdropClick') {
      setOpen(false)
    }
  }
  const onAddCategory = async () => {
    try {
      setOpen(false)
      const obj = {
        category,
      }

      const categoryData = await Api().category.create(obj)
      await router.push(`/write/`)
    } catch (err) {
      console.warn(err)
    }
  }
  return (
    <div>
      <Button onClick={handleClickOpen} className={styles.plassButton}>
        <AddIcon />
      </Button>
      <Dialog disableEscapeKeyDown open={open} onClose={handleClose}>
        <DialogTitle>Добавить категорию</DialogTitle>
        <Input
          value={category}
          onChange={(e) => setCategory(e.target.value)}
          className={styles.categoryField}
          placeholder="Имя Категории"
        />
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={onAddCategory}>Ok</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
