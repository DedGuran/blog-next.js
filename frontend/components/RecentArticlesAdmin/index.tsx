import React, { useEffect, useState } from 'react'
import { PostItem } from '../../utils/api/types'
import { BsThreeDotsVertical } from 'react-icons/bs'
import { IconButton, Link } from '@material-ui/core'
import DeleteIcon from '@mui/icons-material/Clear'
import { Api } from '../../utils/api'
import EditIcon from '@mui/icons-material/Edit'
import { useRouter } from 'next/router'
interface ResentArticlesProps {
  postsData: PostItem[]
}

export const RecentArticlesAdmin: React.FC<ResentArticlesProps> = ({
  postsData,
}) => {
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const onClickRemove = async (id) => {
    if (window.confirm('Вы действительно хотите удалить стать?')) {
      try {
        setIsLoading(true)
        const post = await Api().post.remove(id)
        await router.push(`/admin`)
      } catch (err) {
        console.warn(err)
      } finally {
        setIsLoading(false)
      }
    }
  }

  return (
    <>
      {/* <div className='w-full col-span-1 relative lg:h-[70vh] h-[50vh] m-auto border rounted-lg bg-white overflow-scroll'>
      
      <ul>
        {postsData.map(obj=>
          <li key={obj.id} className='bg-gray-50 hover:bg-gray-100 rounded-lg my-3 p-2 flex items-center cursor-pointer'>
            <div className='bg-purple-100 rounded-lg'>
            <img src={obj.body.find(obj => obj.type === 'image')?.data?.file.url} alt={obj.title} style={{ height: "10vh", width: "10vh" }}  />
            </div>
            <div className='pl-4'>
              <p className='text-gray-800 font-bold'>{obj.title}</p>
              <p className='text-gray-400 text-sm'>{obj.user.fullName}</p>
              <p className='text-gray-400 text-sm'>{new Date(obj.createdAt).toLocaleDateString('ru-RU', {
                            month: '2-digit',
                            day: '2-digit',
                            year: 'numeric'
                        })}</p>
            </div>
            <p className='lg:flex mb:hidden absolute right-6 text-sm'><BsThreeDotsVertical/></p>
          </li>
        )}
      </ul>
    </div> */}

      <div className="bg-gray-100 min-h-screen">
        <div className="p-4">
          <div className="w-full m-auto p-4 border rounded-lg bg-white overflow-y-auto">
            <div className="my-3 p-2 flex items-center justify-between cursor-pointer">
              <span>Статья</span>
              <span className="sm:text-left text-right">Просмотры</span>
              <span className="sm:text-left text-right">Категория</span>
              <span className="sm:text-left text-right">Пользователь</span>
              <span className="sm:text-left text-right">Опции</span>
            </div>
            <ul>
              {postsData.map((obj) => (
                <li
                  key={obj.id}
                  className="bg-gray-50 hover:bg-gray-100 rounded-lg my-3 p-2 flex items-center justify-between cursor-pointer"
                >
                  <div className="MuiAvatar-root MuiAvatar-circular flex items-center">
                    <img
                      alt={obj.title}
                      src={
                        obj.body.find((obj) => obj.type === 'image')?.data?.file
                          .url
                      }
                      style={{ height: '10vh', width: '10vh' }}
                    />
                    <p className="hidden md:flex pl-4">{obj.title}</p>
                  </div>
                  <p className="text-gray-600 sm:text-left text-right">
                    {obj.views}
                  </p>
                  <p className="text-gray-600 sm:text-left text-right">
                    {obj.category.category}
                  </p>
                  <p className="text-gray-600 sm:text-left text-right">
                    {obj.user.fullName}
                  </p>
                  <div className="flex">
                    <p className="text-gray-600 sm:text-left text-right">
                      <IconButton
                        onClick={() => {
                          onClickRemove(obj.id)
                        }}
                        color="secondary"
                      >
                        <DeleteIcon />
                      </IconButton>
                    </p>
                    <p className="text-gray-600 sm:text-left text-right">
                      <Link href={`/write/${obj.id}`}>
                        <IconButton color="primary">
                          <EditIcon />
                        </IconButton>
                      </Link>
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </>
  )
}
