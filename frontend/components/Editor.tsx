import React, { useEffect, useState } from 'react'
import EditorJS, { OutputData } from '@editorjs/editorjs'
import ImageTool from '@editorjs/image'

import Header from '@editorjs/header'
import List from '@editorjs/list'
import { Api } from '../utils/api'

interface EditorProps {
  onChange: (blocks: OutputData['blocks']) => void
  initialBlocks?: OutputData['blocks']
  type: string
}
export const Editor: React.FC<EditorProps> = ({
  onChange,
  initialBlocks,
  type,
}) => {
  const [editorInstance, setEditorInstance] = useState(null)
  let imagesUploaded = []
  //const [imagesUploaded, setImagesUploaded]: any[] = useState([])

  useEffect(() => {
    setTimeout(() => {
      if (
        imagesUploaded.length == 0 &&
        document.querySelectorAll('.image-tool__image-picture').length > 0
      ) {
        document
          .querySelectorAll('.image-tool__image-picture')
          .forEach((el) => {
            imagesUploaded.push(el.getAttribute('src'))
          })
      }
    }, 1000)
  }, [type])
  //     useEffect(() => {

  //         const editor = new EditorJS({
  //             holder: 'editor',
  //             data: {
  //                 blocks: initialBlocks
  //             },
  //             placeholder: 'Введите текст вашей статьи',
  //             async onChange(ev) {
  //                 const { blocks } = await editor.save();

  //                 onChange(blocks)

  //                 if (imagesUploaded.length == 0 && document.querySelectorAll('.image-tool__image-picture').length > 0) {

  //                     document.querySelectorAll('.image-tool__image-picture').forEach(el => {
  //                         imagesUploaded.push(el.getAttribute('src'))
  //                     })
  //                 }
  //                 //remove image from server
  //                 if (imagesUploaded.length >= document.querySelectorAll('.image-tool__image-picture').length) {

  //                     imagesUploaded.forEach(async (img, i) => {
  //                         let currentImages = []
  //                         document.querySelectorAll('.image-tool__image-picture').forEach(el => {
  //                             currentImages.push(el.getAttribute('src'))
  //                         })

  //                         if (!currentImages.includes(img)) {

  //                             try {
  //                                 return Api().post.removeImage(img.split('/').slice(-1).join()).then((data) => {
  //                                     imagesUploaded.filter(obj => obj != data.urlName)
  //                                 });
  //                             } catch (err) {
  //                                 console.log(err.message)
  //                             }
  //                         }
  //                     })
  //                 }
  //             },
  //             tools: {
  //                 header: {
  //                     class: Header,
  //                     config: {
  //                         placeholder: 'Enter a header',
  //                         levels: [2, 3, 4],
  //                         defaultLevel: 3
  //                     }
  //                 },
  //                 list: {
  //                     class: List,
  //                     inlineToolbar: true
  //                 },
  //                 image: {
  //                     class: ImageTool,

  //                     config: {
  //                         endpoints: {
  //                             byFile: 'http://localhost:4000/posts/uploadImage', // Your backend file uploader endpoint

  //                         },
  //                         uploader: {
  //                             uploadByFile(file) {

  //                                 return Api().post.imageUpload(file).then((data) => {

  //                                     imagesUploaded.push(data.file.url)
  //                                     return {
  //                                         success: 1,
  //                                         file: {
  //                                             url: data.file.url,

  //                                         }
  //                                     };
  //                                 });
  //                             },
  //                         },

  //                         field: 'image',
  //                         types: 'image/*'
  //                     }
  //                 },
  //             },

  //         });
  // //
  //     return () => {
  //         editor.isReady.then(() => {
  //             editor.destroy();
  //         })
  //             .catch(e => console.error('ERROR editor cleanup', e));
  //     }
  // }, []);
  const saveEditorData = async () => {
    const { blocks } = await editorInstance.save()
    onChange(blocks)
    if (
      imagesUploaded.length == 0 &&
      document.querySelectorAll('.image-tool__image-picture').length > 0
    ) {
      document.querySelectorAll('.image-tool__image-picture').forEach((el) => {
        imagesUploaded.push(el.getAttribute('src'))
      })
    }
    //remove image from server
    if (
      imagesUploaded.length >=
      document.querySelectorAll('.image-tool__image-picture').length
    ) {
      imagesUploaded.forEach(async (img, i) => {
        let currentImages = []
        document
          .querySelectorAll('.image-tool__image-picture')
          .forEach((el) => {
            currentImages.push(el.getAttribute('src'))
          })

        if (!currentImages.includes(img)) {
          try {
            return Api()
              .post.removeImage(img.split('/').slice(-1).join())
              .then((data) => {
                imagesUploaded.filter((obj) => obj != data.urlName)
              })
          } catch (err) {
            console.log(err.message)
          }
        }
      })
    }
  }
  const initializeEditor = () => {
    const editor = new EditorJS({
      holder: 'editor',
      data: {
        blocks: initialBlocks,
      },
      placeholder: 'Введите текст вашей статьи',
      onChange: (e) => {
        console.log('Content has changed!')
      },
      // async onChange(ev) {
      //     console.log('111')
      //     const { blocks } = await editor.save();

      //     onChange(blocks)

      //     if (imagesUploaded.length == 0 && document.querySelectorAll('.image-tool__image-picture').length > 0) {

      //         document.querySelectorAll('.image-tool__image-picture').forEach(el => {
      //             imagesUploaded.push(el.getAttribute('src'))
      //         })
      //     }
      //     //remove image from server
      //     if (imagesUploaded.length >= document.querySelectorAll('.image-tool__image-picture').length) {

      //         imagesUploaded.forEach(async (img, i) => {
      //             let currentImages = []
      //             document.querySelectorAll('.image-tool__image-picture').forEach(el => {
      //                 currentImages.push(el.getAttribute('src'))
      //             })

      //             if (!currentImages.includes(img)) {

      //                 try {
      //                     return Api().post.removeImage(img.split('/').slice(-1).join()).then((data) => {
      //                         imagesUploaded.filter(obj => obj != data.urlName)
      //                     });
      //                 } catch (err) {
      //                     console.log(err.message)
      //                 }
      //             }
      //         })
      //     }
      // },
      tools: {
        header: {
          class: Header,
          config: {
            placeholder: 'Enter a header',
            levels: [2, 3, 4],
            defaultLevel: 3,
          },
        },
        list: {
          class: List,
          inlineToolbar: true,
        },
        image: {
          class: ImageTool,

          config: {
            endpoints: {
              byFile: 'http://localhost:4000/posts/uploadImage', // Your backend file uploader endpoint
            },
            uploader: {
              uploadByFile(file) {
                return Api()
                  .post.imageUpload(file)
                  .then((data) => {
                    imagesUploaded.push(data.file.url)
                    return {
                      success: 1,
                      file: {
                        url: data.file.url,
                      },
                    }
                  })
              },
            },

            field: 'image',
            types: 'image/*',
          },
        },
      },
    })

    setEditorInstance(editor)
  }
  return (
    <div className="flex flex-col">
      <div id="editor"></div>
      <button
        className="w-1/4 mx-auto justify-center mb-10 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center"
        onClick={initializeEditor}
      >
        Показать редактор
      </button>
      <button
        className="w-1/4 mx-auto justify-center mb-10 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center"
        onClick={saveEditorData}
      >
        Сохранить данные
      </button>
    </div>
  )
}
