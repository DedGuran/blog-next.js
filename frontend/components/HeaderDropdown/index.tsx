import React from 'react'
import ClickAwayListener from '@mui/base/ClickAwayListener'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Divider from '@mui/material/Divider'
import InboxIcon from '@mui/icons-material/Inbox'

import ListItemAvatar from '@mui/material/ListItemAvatar'
import Avatar from '@mui/material/Avatar'
import Typography from '@mui/material/Typography'
import { signOut } from 'next-auth/react'
import Cookies from 'js-cookie'
import { ResponseUser } from '../../utils/api/types'

interface AuthDialogProps {
  onClose: any
  visible: boolean
  userInfo: ResponseUser
}

export const HeaderDropdown: React.FC<AuthDialogProps> = ({
  visible,
  onClose,
  userInfo,
}) => {
  const handleSignOut = () => {
    Cookies.remove('token')
    signOut()
  }

  return visible ? (
    <ClickAwayListener onClickAway={onClose}>
      <Box
        sx={{
          width: '100%',
          maxWidth: 360,
          bgcolor: 'background.paper',
          position: 'absolute',
          top: '70px',
          right: '13%',
          zIndex: '100',
        }}
      >
        <nav aria-label="main mailbox folders">
          <List>
            <ListItem alignItems="center">
              <ListItemAvatar>
                <Avatar alt={userInfo.fullName} src={userInfo.avatar} />
              </ListItemAvatar>
              <ListItemText
                primary=""
                secondary={
                  <React.Fragment>
                    <Typography
                      sx={{ display: 'inline' }}
                      component="span"
                      variant="body2"
                      color="text.primary"
                    >
                      {userInfo.fullName}
                    </Typography>
                  </React.Fragment>
                }
              />
            </ListItem>
            <Divider />
            <ListItem disablePadding>
              <ListItemButton component="a" href={`/profile/${userInfo.id}`}>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Личный кабинет" />
              </ListItemButton>
            </ListItem>
            {userInfo.role == 'admin' ? (
              <ListItem disablePadding>
                <ListItemButton component="a" href={`/admin`}>
                  <ListItemIcon>
                    <InboxIcon />
                  </ListItemIcon>
                  <ListItemText primary="Админ панель" />
                </ListItemButton>
              </ListItem>
            ) : null}
          </List>
        </nav>
        <Divider />
        <nav aria-label="secondary mailbox folders">
          <List>
            <ListItem disablePadding onClick={handleSignOut}>
              <ListItemButton component="a" href="#simple-list">
                <ListItemText primary="Выйти" />
              </ListItemButton>
            </ListItem>
          </List>
        </nav>
      </Box>
    </ClickAwayListener>
  ) : null
}
