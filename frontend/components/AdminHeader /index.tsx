import React, { useState, useEffect } from 'react'
interface AdminHeaderProps {
  title: 'string'
}

export const AdminHeader: React.FC<AdminHeaderProps> = ({ title }) => {
  return (
    <div className="flex justify-between px-4 pt-4">
      <h2>{title}</h2>
      <h2>Добро пожаловать, Андрей</h2>
    </div>
  )
}
