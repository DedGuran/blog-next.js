import React, { useEffect, useState } from 'react'
import { Bar } from 'react-chartjs-2'
import {
  BarElement,
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  Title,
  Tooltip,
} from 'chart.js'

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend)
export const BarChartAdmin = () => {
  const [chartData, setChartData] = useState({
    datasets: [],
  })
  const [chartOptions, setChartOptions] = useState({})
  useEffect(() => {
    return setChartData({
      labels: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
      datasets: [
        {
          label: 'Sales $',
          data: [1200, 2200, 3300, 4400, 5500, 3343],
          borderColor: 'rgd(53,162,235)',
          backgroundColor: 'rgb(53,162,235,0.4)',
        },
      ],
    })
    setChartOptions({
      plagins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Daily Revenue',
        },
      },
      maintainAspectRatio: false,
      responsive: true,
    })
  }, [])
  return (
    <>
      <div className="w-full mb:col-span-2 relative lg:h-[70vh] h-[50vh] m-auto p-4 border bounded-lg bg-white">
        <Bar data={chartData} options={chartOptions} />
      </div>
    </>
  )
}
