import React, { useState } from 'react'
import styles from './RemoveCategoryForm.module.scss'
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogTitle,
  Input,
  InputLabel,
  Select,
  MenuItem,
} from '@material-ui/core'
import FormControl from '@mui/material/FormControl'
import { Api } from '../../utils/api'
import router from 'next/router'
import { ResponseCategory } from '../../utils/api/types'
import RemoveIcon from '@mui/icons-material/Remove'

interface RemoveCategoryFormProps {
  categoryData?: ResponseCategory[]
}

export const RemoveCategoryForm: React.FC<RemoveCategoryFormProps> = ({
  categoryData,
}) => {
  const [open, setOpen] = useState(false)
  const [category, setCategory] = useState(categoryData || '')

  const handleClickOpen = () => {
    setOpen(true)
  }
  const handleClose = (
    event: React.SyntheticEvent<unknown>,
    reason?: string
  ) => {
    if (reason !== 'backdropClick') {
      setOpen(false)
    }
  }
  const onRemoveCategory = async (id) => {
    try {
      setOpen(false)

      const categoryData = await Api().category.remove(id)
      await router.push(`/write/`)
    } catch (err) {
      console.warn(err)
    }
  }

  return (
    <div>
      <Button onClick={handleClickOpen} className={styles.plassButton}>
        <RemoveIcon />
      </Button>
      <Dialog disableEscapeKeyDown open={open} onClose={handleClose}>
        <DialogTitle>Удалить категорию</DialogTitle>
        <div className={styles.categoryForm}>
          <FormControl sx={{ m: 1, minWidth: 120 }}>
            <InputLabel id="demo-simple-select-label">Категории</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={category}
              label="Category"
              onChange={(e) => setCategory(e.target.value)}
            >
              {categoryData?.map((obj) => (
                <MenuItem key={obj.id} value={obj.id}>
                  {obj.category}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
        <DialogActions>
          <Button onClick={handleClose}>Отмена</Button>
          <Button
            onClick={() => {
              onRemoveCategory(category)
            }}
          >
            Удалить
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
