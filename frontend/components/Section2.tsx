import React from 'react'
import { Post } from './_childComponent/Post'
import { selectUserData } from '../redux/slices/users'
import { useAppSelector } from '../redux/hooks'
import { PostItem, ResponseCategory, ResponseTags } from '../utils/api/types'
import { PostCategores } from './_childComponent/PostCategores'
import { PostTags } from './_childComponent/PostTags'

interface SectionProps {
  postsData: PostItem[]
  categoriesData: ResponseCategory[]
  tagsData: ResponseTags
  sectionTitle: string
  handleCategoryChange: (categoryId: string) => void
  handleTagsChange: (tagName: string) => void
}

export const Section2: React.FC<SectionProps> = ({
  postsData,
  categoriesData,
  sectionTitle,
  handleCategoryChange,
  tagsData,
  handleTagsChange,
}) => {
  const userData = useAppSelector(selectUserData)
  return (
    <section className="contant mx-auto mb:px-20 py-2">
      <h1 className="font-bold text-4xl py-12 text-center">{sectionTitle}</h1>
      <PostCategores
        categoriesData={categoriesData}
        handleCategoryChange={handleCategoryChange}
      />
      <PostTags tagsData={tagsData} handleTagsChange={handleTagsChange} />
      <div className="grid md:grid-cols-2 lg:grid-cols-3 gap-14">
        {Array.isArray(postsData) &&
          postsData?.map((obj) => (
            <Post
              key={obj.id}
              userRole={obj.user.role}
              categoryId={obj.category?.id}
              subtitle={obj.subtitle}
              id={obj.id}
              title={obj.title}
              category={obj.category?.category}
              description={obj.description}
              image={
                obj.body.find((obj) => obj.type === 'image')?.data?.file.url
              }
              date={obj.createdAt}
              isEditable={userData?.id === obj.user.id}
              userName={obj.user.fullName}
              userFoto={obj.user.avatar}
              userStatus={obj.user.status}
            />
          ))}
      </div>
    </section>
  )
}
