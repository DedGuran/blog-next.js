import React, { useState } from 'react'
import Input from '@material-ui/core/Input'
import styles from './AddCommentForm.module.scss'
import { Button } from '@material-ui/core'
import { Api } from '../../utils/api'
import { useAppSelector } from '../../redux/hooks'
import { selectUserData, setUserData } from '../../redux/slices/users'

import { CommentItem } from '../../utils/api/types'
import { useDispatch } from 'react-redux'
import { fetchAddComment } from '../../redux/slices/comments'

interface AddCommentFormProps {
  postId: number

  //onSuccessAdd: (obj: CommentItem) => void
}

export const AddCommentForm: React.FC<AddCommentFormProps> = ({ postId }) => {
  const dispatch = useDispatch()
  const userData = useAppSelector(selectUserData)
  const [clicked, setClicked] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [text, setText] = useState('')
  const [active, setActive] = useState(userData?.role == 'admin' ? true : false)

  const onAddComment = async () => {
    try {
      setIsLoading(true)
      //const comment = await Api().comment.create({ postId, text, active })
      dispatch(fetchAddComment({ postId, text, active }))
      setClicked(false)
      setText('')
    } catch (err) {
      console.warn('Add comments', err)
      alert('Ошибка при отправке комментария')
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <div className={styles.form}>
      <Input
        disabled={isLoading}
        onChange={(e) => setText(e.target.value)}
        value={text}
        onFocus={() => setClicked(true)}
        minRows={clicked ? 5 : 1}
        classes={{ root: styles.fieldRoot }}
        placeholder="Написать комментарий..."
        fullWidth
        multiline
      />
      {clicked && (
        <Button
          disabled={isLoading}
          onClick={onAddComment}
          className={styles.addButton}
          variant="contained"
          color="primary"
        >
          Опубликовать
        </Button>
      )}
    </div>
  )
}
