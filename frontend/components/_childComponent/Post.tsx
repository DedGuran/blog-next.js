import React, { useState } from 'react'
import Link from 'next/link'
import AuthorSection from './AuthorSection'
import FolderOpenSharpIcon from '@mui/icons-material/FolderOpenSharp'
import FolderSpecialSharpIcon from '@mui/icons-material/FolderSpecialSharp'
import DeleteIcon from '@mui/icons-material/Clear'
import EditIcon from '@mui/icons-material/Edit'
import IconButton from '@mui/material/IconButton'

import { Api } from '../../utils/api'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { fetchAddFavorites } from '../../redux/slices/favorites'

interface PostProps {
  image: string
  category: string
  date: string
  title: string
  description: string
  isEditable: boolean
  id: number
  subtitle: string
  userName: string
  userFoto: string
  userRole: string
  categoryId: number
  userStatus: string
}
export const Post: React.FC<PostProps> = ({
  id,
  image,
  category,
  date,
  categoryId,
  title,
  isEditable,
  subtitle,
  userName,
  userFoto,
  userRole,
  userStatus,
}) => {
  const dispatch = useDispatch()
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const [addFavorites, setAddFavorites] = useState(false)
  const onClickRemove = async (id) => {
    if (window.confirm('Вы действительно хотите удалить стать?')) {
      try {
        setIsLoading(true)
        const post = await Api().post.remove(id)
        await router.push(`/`)
      } catch (err) {
        console.warn(err)
      } finally {
        setIsLoading(false)
      }
    }
  }
  const onClickAddFavorites = async (id) => {
    try {
      setAddFavorites(true)
      dispatch(fetchAddFavorites({ postId: id }))
    } catch (err) {
      console.warn(err)
    }
  }
  const onClickRemoveFavorites = async (id) => {
    try {
      setAddFavorites(false)
    } catch (err) {
      console.warn(err)
    }
  }
  return (
    <div className="item">
      <div className="images"></div>
      <div className="info flex justify-center flex-col py-4 relative">
        {isEditable && (
          <div className="absolute left-[10px] top-[10px] opacity-0 hover:opacity-100 bg-slate-50 rounded-lg">
            <Link href={`/write/${id}`}>
              <IconButton color="primary">
                <EditIcon />
              </IconButton>
            </Link>
            <IconButton
              onClick={() => {
                onClickRemove(id)
              }}
              color="secondary"
            >
              <DeleteIcon />
            </IconButton>
          </div>
        )}
        <Link href={`/post/${id}`} className="mx-auto">
          <img
            src={image}
            className="rounded"
            style={{ height: '50vh', width: '90vh' }}
          />
        </Link>
      </div>
      <div className="cat">
        <Link
          href={`category/${categoryId}`}
          className="text-orange-600 hover:text-orange-800 mr-10"
        >
          {category}
        </Link>
        <Link href={`/`} className="text-grey-800 hover:text-grey-600 mr-10">
          {new Date(date).toLocaleDateString('ru-RU', {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric',
          })}
        </Link>
        {addFavorites ? (
          <Link
            href={`/`}
            className="text-grey-800 hover:text-grey-600"
            onClick={() => {
              onClickRemoveFavorites(id)
            }}
          >
            <FolderSpecialSharpIcon />
          </Link>
        ) : (
          <Link
            href={`/`}
            className="text-grey-800 hover:text-grey-600"
            onClick={() => {
              onClickAddFavorites(id)
            }}
          >
            <FolderOpenSharpIcon />
          </Link>
        )}
      </div>
      <div className="title">
        <Link
          href={`/post/${id}`}
          className="text-xl font-bold text-gray-800 hover:text-gray-600"
        >
          {title}
        </Link>
      </div>
      <p className="text-gray-500 py-3">{subtitle}</p>
      <AuthorSection
        userName={userName}
        userFoto={userFoto}
        userRole={userRole}
        userStatus={userStatus}
      />
    </div>
  )
}
