import React from 'react'
import Link from 'next/link'
import AuthorSection from './AuthorSection'

interface PostProps {
  id: number
  image: string
  category: string
  date: string
  description: string
  title: string
  userName: string
  userFoto: string
  userRole: string
  categoryId: string
  userStatus: string
}
export const CategoryPost: React.FC<PostProps> = ({
  id,
  image,
  category,
  date,
  categoryId,
  title,
  userName,
  userFoto,
  userRole,
  userStatus,
}) => {
  return (
    <div className="flex grap-5">
      <div className="image flex flex-col justify-start mr-10">
        <Link href={`/`}>
          <img
            className="rounded"
            style={{ height: '30vh', width: '90vh' }}
            src={image}
            height={300}
            width={250}
          />
        </Link>
      </div>
      <div className="info flex justify-center flex-col">
        <div className="cat">
          <Link
            href={`/category/${categoryId}`}
            className="text-orange-600 hover:text-orange-800"
          >
            {category}
          </Link>
          <Link href={`/`} className="text-grey-800 hover:text-grey-600 ml-10">
            {new Date(date).toLocaleDateString('ru-RU', {
              month: '2-digit',
              day: '2-digit',
              year: 'numeric',
            })}
          </Link>
        </div>
        <div className="title">
          <Link
            href={`/post/${id}`}
            className="text-xl font-bold text-gray-800 hover:text-gray-600"
          >
            {title}
          </Link>
        </div>
        <AuthorSection
          userName={userName}
          userFoto={userFoto}
          userRole={userRole}
          userStatus={userStatus}
        />
      </div>
    </div>
  )
}
