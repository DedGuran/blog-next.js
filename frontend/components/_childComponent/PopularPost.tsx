import React from 'react'
import Link from 'next/link'
import AuthorSection from './AuthorSection'

interface PostProps {
  image: string
  category: string
  date: string
  description: string
  title: string
}
export const PopularPost: React.FC<PostProps> = ({
  image,
  category,
  date,
  description,
  title,
}) => {
  return (
    <div className="grid">
      <div className="images"></div>
      <div className="info flex justify-center flex-col py-4">
        <Link href={`/`}>
          <img src={image} height={600} width={400} />
        </Link>
      </div>
      <div className="cat">
        <Link href={`/`} className="text-orange-600 hover:text-orange-800">
          {category}
        </Link>
        <Link href={`/`} className="text-grey-800 hover:text-grey-600">
          {date}
        </Link>
      </div>
      <div className="title">
        <Link
          href={`/`}
          className="text-3xl md:text-4xl font-bold text-gray-800 hover:text-gray-600"
        >
          Test Title
        </Link>
      </div>
      <p className="text-gray-500 py-3">{description}</p>
      <AuthorSection />
    </div>
  )
}
