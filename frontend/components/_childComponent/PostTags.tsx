import React, { useState } from 'react'
import Button from '@mui/material/Button'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { useDispatch } from 'react-redux'

interface PostTagsProps {
  tagsData: any
  handleTagsChange: (tagName: string) => void
}
export const PostTags: React.FC<PostTagsProps> = ({
  tagsData,
  handleTagsChange,
}) => {
  const dispatch = useDispatch()
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)

  const open = Boolean(anchorEl)
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  const addPostsByTagName = (name) => {
    handleTagsChange(name)

    handleClose()
  }
  //const transformedTagsArray = tagsData.flatMap(item => item.split(','));

  const modifiedTagsArray = tagsData
    .map((item) => item.split(',').map((subItem) => subItem.trim())) // Split and trim subitems
    .flat() // Flatten the array of arrays
    .filter((item) => item !== '') // Remove empty strings
    .filter((item, index, array) => array.indexOf(item) === index) // Remove duplicates

  return (
    <div className="inline ml-10">
      <Button
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        variant="outlined"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        Теги
      </Button>

      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        {Array.isArray(modifiedTagsArray) &&
          modifiedTagsArray?.map((obj) => (
            <MenuItem key={obj.id} onClick={() => addPostsByTagName(obj)}>
              <Button size="small">{obj}</Button>
            </MenuItem>
          ))}
      </Menu>
    </div>
  )
}
