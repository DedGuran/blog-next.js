// import React, { useState } from 'react'
// import List from '@mui/material/List';
// import ListItem from '@mui/material/ListItem';
// import ListItemText from '@mui/material/ListItemText';
// import ListItemAvatar from '@mui/material/ListItemAvatar';
// import Avatar from '@mui/material/Avatar';
// import ImageIcon from '@mui/icons-material/Image';
// import WorkIcon from '@mui/icons-material/Work';
// import BeachAccessIcon from '@mui/icons-material/BeachAccess';

// interface CommentsItemProps {
//     id: number,
//     avatar:string
//     text:string
// }
// export const CommentsItem: React.FC<CommentsItemProps> = ({id,avatar,text  }) => {

//     return (
//         <ListItem button key={id}>
//         <ListItemAvatar>
//           <Avatar alt="Profile Picture" src={avatar} />
//         </ListItemAvatar>
//         <ListItemText secondary={text} />
//       </ListItem>
//     )
// }
import * as React from 'react'
import Box from '@mui/material/Box'
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid'

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 90 },
  {
    field: 'Text',
    headerName: 'Text',
    width: 150,
    editable: true,
  },
  {
    field: 'LikeCount',
    headerName: 'LikeCount',
    type: 'number',
    width: 110,
    editable: true,
  },
]

const rows = [
  { id: 1, Text: 'Snow', LikeCount: 11 },
  { id: 2, Text: 'Lannister', LikeCount: 11 },
]

export const CommentsItem: React.FC<any> = ({ id, avatar, text }) => {
  return (
    <Box sx={{ height: 400, width: '100%' }}>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: {
              pageSize: 5,
            },
          },
        }}
        pageSizeOptions={[5]}
        checkboxSelection
        disableRowSelectionOnClick
      />
    </Box>
  )
}
