import React, { useEffect } from 'react'
import { PostRalated } from './PostRalated'

export const Related = ({ popularPosts }) => {
  return (
    <section className="container mx-auto md:px-2 py-10 w-1/2">
      <h1 className="font-bold text-3xl py-10">Популярные посты</h1>

      <div className="flex flex-col gap-10">
        {popularPosts.items?.map((obj) => (
          <PostRalated
            key={obj.id}
            id={obj.id}
            categoryId={obj.category.id}
            image={obj.body.find((obj) => obj.type === 'image')?.data?.file.url}
            category={obj.category.category}
            date={obj.createdAt}
            title={obj.title}
            userName={obj.user.fullName}
            userRole={obj.user.role}
            userFoto={obj.user.avatar}
            userStatus={obj.user.status}
          />
        ))}
      </div>
    </section>
  )
}
