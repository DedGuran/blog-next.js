import React, { useState } from 'react'
import Link from 'next/link'

import DeleteIcon from '@mui/icons-material/Clear'
import EditIcon from '@mui/icons-material/Edit'
import IconButton from '@mui/material/IconButton'

import { Api } from '../../utils/api'
import { useRouter } from 'next/router'
import AuthorSection from './AuthorSection'

interface HomeSliderProps {
  image: string
  category: string
  date: string
  title: string
  description: string
  isEditable: boolean
  id: number
  subtitle: string
  userName: string
  userFoto: string
  userRole: string
  categoryId: number
  userStatus: string
}
export const HomeSlider: React.FC<HomeSliderProps> = ({
  image,
  category,
  categoryId,
  date,
  title,
  isEditable,
  id,
  subtitle,
  userName,
  userFoto,
  userRole,
  userStatus,
}) => {
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const onClickRemove = async (id) => {
    if (window.confirm('Вы действительно хотите удалить стать?')) {
      try {
        setIsLoading(true)
        const post = await Api().post.remove(id)
        await router.push(`/`)
      } catch (err) {
        console.warn(err)
      } finally {
        setIsLoading(false)
      }
    }
  }
  return (
    <div className="grid md:grid-cols-2 align-items:flex-start">
      <div className="image">
        {isEditable && (
          <div className="absolute left-[10px] top-[10px] opacity-0 hover:opacity-100 bg-slate-50 rounded-lg">
            <Link href={`/write/${id}`}>
              <IconButton color="primary">
                <EditIcon />
              </IconButton>
            </Link>
            <IconButton
              onClick={() => {
                onClickRemove(id)
              }}
              color="secondary"
            >
              <DeleteIcon />
            </IconButton>
          </div>
        )}
        <Link href={`/post/${id}`}>
          <img src={image} style={{ height: '50vh', width: '100vh' }} />
        </Link>
      </div>
      <div className="info flex justify-center flex-col ml-40">
        <div className="cat">
          <Link
            href={`/category/${categoryId}`}
            className="text-orange-600 hover:text-orange-800"
          >
            {category}
          </Link>
          <Link href={`/`} className="text-grey-800 hover:text-grey-600 ml-2">
            {new Date(date).toLocaleDateString('ru-RU', {
              month: '2-digit',
              day: '2-digit',
              year: 'numeric',
            })}
          </Link>
        </div>
        <div className="title">
          <Link
            href={`/post/${id}`}
            className="text-3xl md:text-6xl font-bold text-gray-800 hover:text-gray-600"
          >
            {title}
          </Link>
        </div>
        <p className="text-gray-500 py-3">{subtitle}</p>
        <AuthorSection
          userName={userName}
          userFoto={userFoto}
          userRole={userRole}
          userStatus={userStatus}
        />
      </div>
    </div>
  )
}
