import React, { useState } from 'react'
import Button from '@mui/material/Button'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import { ResponseCategory } from '../../utils/api/types'
import { useDispatch } from 'react-redux'

interface PostCategoresProps {
  categoriesData: ResponseCategory[]
  handleCategoryChange: (categoryId: string) => void
}
export const PostCategores: React.FC<PostCategoresProps> = ({
  categoriesData,
  handleCategoryChange,
}) => {
  const dispatch = useDispatch()
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  const addPostsByCategoryId = (id) => {
    handleCategoryChange(id)

    handleClose()
  }
  return (
    <div className="inline">
      <span style={{ paddingBottom: '10px' }}>
        <Button
          id="basic-button"
          aria-controls={open ? 'basic-menu' : undefined}
          aria-haspopup="true"
          variant="outlined"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleClick}
        >
          Категории
        </Button>
      </span>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        {Array.isArray(categoriesData) &&
          categoriesData?.map((obj) => (
            <MenuItem key={obj.id} onClick={() => addPostsByCategoryId(obj.id)}>
              <Button size="small">{obj.category}</Button>
            </MenuItem>
          ))}
      </Menu>
    </div>
  )
}
