import React from 'react'
import Link from 'next/link'
import { Avatar } from '@material-ui/core'

export default function AuthorSection({
  userName,
  userFoto,
  userRole,
  userStatus,
}) {
  return (
    <div className="author flex py-5">
      <Avatar className="" alt={userName} src={userFoto ? userFoto : ''} />
      <div className="flex flex-col justify-center px-4 h-14">
        <Link
          href={'/'}
          className="text-mb font-bold text-gray-800 hover:text-gray-600"
        >
          {userName}
        </Link>
        <span className="text-sm text-gray-500">{userStatus}</span>
      </div>
    </div>
  )
}
