import React, { useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
} from '@mui/material'
import { FormField } from '../FormField'
import Alert from '@material-ui/lab/Alert'

const FeedbackForm = ({ open, onClose }) => {
  const [errorMessages, setErrorMessages] = useState('')
  const form = useForm({
    mode: 'onChange',
    //resolver: yupResolver(LoginFormSchema),
  })
  //const { handleSubmit, register, formState: { errors } } = useForm();

  const onSubmit = (data) => {
    console.log('11', data) // Handle form submission logic with form data
    onClose() // Close the dialog after form submission
  }

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Форма обратной связи</DialogTitle>
      <DialogContent>
        <FormProvider {...form}>
          <form onSubmit={form.handleSubmit(onSubmit)}>
            <FormField name="Имя" label="Имя" rows={1} />
            <FormField name="email" label="Почта" rows={1} />
            <FormField name="Сообщение" label="Сообщение" rows={4} />
            {errorMessages ? (
              <Alert severity="error" className="mb-20">
                {errorMessages}
              </Alert>
            ) : null}
            <DialogActions>
              <Button onClick={onClose}>Отменить</Button>
              <Button type="submit">Отправить</Button>
            </DialogActions>
          </form>
        </FormProvider>
      </DialogContent>
    </Dialog>
  )
}

export default FeedbackForm
