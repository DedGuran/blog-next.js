import React, { CSSProperties } from 'react'
import { IconButton } from '@material-ui/core'
import {
  ModeCommentOutlined as CommentsIcon,
  RepeatOutlined as RepostIcon,
  BookmarkBorderOutlined as FavoriteIcon,
  ShareOutlined as ShareIcon,
} from '@material-ui/icons'
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye'
import styles from './PostActions.module.scss'

interface PostActionsProps {
  views: number
  commentsLength: number
}
export const PostActions: React.FC<PostActionsProps> = ({
  views,
  commentsLength,
}) => {
  return (
    <ul className={styles.ulContainer}>
      <li>
        <IconButton>
          <RemoveRedEyeIcon />
          <div className={styles.iconText}>{views}</div>
        </IconButton>
      </li>
      <li>
        <IconButton>
          <CommentsIcon />
          <div className={styles.iconText}>{commentsLength}</div>
        </IconButton>
      </li>
      <li>
        <IconButton>
          <RepostIcon />
        </IconButton>
      </li>
      <li>
        <IconButton>
          <FavoriteIcon />
        </IconButton>
      </li>
    </ul>
  )
}
