import React, { useState } from 'react'
import Alert from '@material-ui/lab/Alert'
import { setCookie } from 'nookies'
import { Button } from '@material-ui/core'
import { useForm, FormProvider } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup/dist/yup'
import { RegisterFormSchema } from '../../../utils/validations'
import { FormField } from '../../FormField'
import { CreateUserDto } from '../../../utils/api/types'
import { UserApi } from '../../../utils/api/user'
import { setUserData } from '../../../redux/slices/users'
import { useAppDispatch } from '../../../redux/hooks'
import { Api } from '../../../utils/api'

interface LoginFormProps {
  onOpenRegister: () => void
  onOpenLogin: () => void
}

export const RegisterForm: React.FC<LoginFormProps> = ({
  onOpenRegister,
  onOpenLogin,
}) => {
  const dispatch = useAppDispatch()
  const [errorMessages, setErrorMessages] = useState('')
  const form = useForm({
    mode: 'onChange',
    //resolver: yupResolver(RegisterFormSchema),
  })

  const onSubmit = async (dto: CreateUserDto) => {
    try {
      const data = await Api().user.register(dto)

      setCookie(null, 'token', data.token, {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      })
      setErrorMessages('')
      dispatch(setUserData(data))
    } catch (err) {
      console.warn('Ошибка при авторизации')
      if (err.response) {
        setErrorMessages(err.response.data.message)
      }
    }
  }

  return (
    <div>
      <FormProvider {...form}>
        <FormField name="fullName" label="Имя и фамилия" />
        <FormField name="email" label="Почта" />
        <FormField name="password" label="Пароль" />
        {errorMessages ? (
          <Alert severity="error" className="mb-20">
            {errorMessages}
          </Alert>
        ) : null}
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <div className="d-flex align-center justify-between">
            <Button
              disabled={!form.formState.isValid || form.formState.isSubmitting}
              onClick={onOpenRegister}
              type="submit"
              color="primary"
              variant="contained"
            >
              Зарегистрироваться
            </Button>
            <Button onClick={onOpenLogin} color="primary" variant="text">
              Войти
            </Button>
          </div>
        </form>
      </FormProvider>
    </div>
  )
}
function dispatch(arg0: any) {
  throw new Error('Function not implemented.')
}
