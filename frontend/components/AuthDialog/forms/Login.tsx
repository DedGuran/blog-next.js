import React, { useState } from 'react'
import Alert from '@material-ui/lab/Alert'
import { Button } from '@material-ui/core'
import { useForm, FormProvider } from 'react-hook-form'

import { LoginFormSchema } from '../../../utils/validations'
import { FormField } from '../../FormField'
import { LoginDto } from '../../../utils/api/types'
import { UserApi } from '../../../utils/api/user'
import { setCookie } from 'nookies'
import { useAppDispatch } from '../../../redux/hooks'
import { setUserData } from '../../../redux/slices/users'
import { Api } from '../../../utils/api'
//import { yupResolver } from '@hookform/resolvers/yup/dist/yup';

interface LoginFormProps {
  onOpenRegister: () => void
}

export const LoginForm: React.FC<LoginFormProps> = ({ onOpenRegister }) => {
  const dispatch = useAppDispatch()
  const [errorMessages, setErrorMessages] = useState('')
  const form = useForm({
    mode: 'onChange',
    //resolver: yupResolver(LoginFormSchema),
  })

  const onSubmit = async (dto: LoginDto) => {
    try {
      const data = await Api().user.login(dto)

      setCookie(null, 'token', data.token, {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      })
      setErrorMessages('')
      dispatch(setUserData(data))
    } catch (err) {
      console.warn('Ошибка при авторизации')
      if (err.response) {
        setErrorMessages(err.response.data.message)
      }
    }
  }
  return (
    <div>
      <FormProvider {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)}>
          <FormField name="email" label="Почта" />
          <FormField name="password" label="Пароль" />
          {errorMessages ? (
            <Alert severity="error" className="mb-20">
              {errorMessages}
            </Alert>
          ) : null}
          <div className="d-flex align-center justify-between">
            <Button
              disabled={!form.formState.isValid || form.formState.isSubmitting}
              type="submit"
              color="primary"
              variant="contained"
            >
              Войти
            </Button>
            <Button onClick={onOpenRegister} color="primary" variant="text">
              Регистрация
            </Button>
          </div>
        </form>
      </FormProvider>
    </div>
  )
}
