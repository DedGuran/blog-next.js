import React, { useState, useRef } from 'react'
import styles from './TagsCategoryMenu.module.scss'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import { useCategory } from '../../hooks/useCategory'

interface TagsCategoryMenuProps {}

export const TagsCategoryMenu: React.FC<TagsCategoryMenuProps> = ({}) => {
  const { category, setCategory } = useCategory()
  const [tagsOpen, setTagsOpen] = useState(0)
  const [categoryOpen, setCategoryOpen] = useState(0)
  const categoryMenu = useRef(null)
  const tagsMenu = useRef(null)

  const openTags = () => {
    setTagsOpen(1)
  }
  const openCategory = () => {
    setCategoryOpen(1)
  }
  const closeCategoryMenu = (e) => {
    if (
      categoryOpen &&
      categoryMenu.current &&
      !categoryMenu.current.contains(e.target)
    ) {
      setCategoryOpen(0)
    }
  }
  const closeTagMenu = (e) => {
    if (tagsOpen && tagsMenu.current && !tagsMenu.current.contains(e.target)) {
      setTagsOpen(0)
    }
  }
  if (typeof window !== 'undefined') {
    document.addEventListener('mousedown', closeCategoryMenu)
    document.addEventListener('mousedown', closeTagMenu)
  }
  console.log('test', category)
  return (
    <>
      <div ref={tagsMenu} className={styles.tagsText} onClick={openTags}>
        Теги{' '}
        <span>
          <KeyboardArrowDownIcon />
        </span>
      </div>
      <ul className={tagsOpen ? 'tagContainer' : 'contentNone'}>
        <li>
          <a href="#">Хакимов</a>
        </li>
        <li>
          <a href="#">Лекции</a>
        </li>
        <li>
          <a href="#">Кулинария</a>
        </li>
      </ul>
      <div
        ref={categoryMenu}
        className={styles.categoryText}
        onClick={openCategory}
      >
        Категории{' '}
        <span>
          <KeyboardArrowDownIcon />
        </span>
      </div>
      <ul className={categoryOpen ? 'categoryContainer' : 'contentNone'}>
        {category.map((obj) => (
          <li>
            <a href="#">{obj.category}</a>
          </li>
        ))}
        {/* <li><a href="#">Хакимов</a></li>
                <li><a href="#">Лекции</a></li>
                <li><a href="#">Кулинария</a></li> */}
      </ul>
    </>
  )
}
