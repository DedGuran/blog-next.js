import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { PopularPost } from './_childComponent/PopularPost'
export default function Section3() {
  return (
    <section className="contant mx-auto mb:px-20 py-2">
      <h1 className="font-bold text-4xl py-12 text-center">Самые популярные</h1>
      <Swiper
        className="xl:container"
        slidesPerView={2}
        autoplay={{ delay: 5000 }}
      >
        <SwiperSlide>
          <PopularPost
            image={
              'https://buketonline.su/image/cache/catalog/ijul2020/22675f4f-3bae-463a-ad5e-2766042660a9-500x500.jpg'
            }
            category={'Business'}
            date={'-July3, 2022'}
            description={'test text'}
            title={'title1'}
          />
        </SwiperSlide>
        <SwiperSlide>
          <PopularPost
            image={
              'https://buketonline.su/image/cache/catalog/ijul2020/22675f4f-3bae-463a-ad5e-2766042660a9-500x500.jpg'
            }
            category={'Business'}
            date={'-July3, 2022'}
            description={'test text'}
            title={'title1'}
          />
        </SwiperSlide>
        <SwiperSlide>
          <PopularPost
            image={
              'https://buketonline.su/image/cache/catalog/ijul2020/22675f4f-3bae-463a-ad5e-2766042660a9-500x500.jpg'
            }
            category={'Business'}
            date={'-July3, 2022'}
            description={'test text'}
            title={'title1'}
          />
        </SwiperSlide>
        <SwiperSlide>
          <PopularPost
            image={
              'https://buketonline.su/image/cache/catalog/ijul2020/22675f4f-3bae-463a-ad5e-2766042660a9-500x500.jpg'
            }
            category={'Business'}
            date={'-July3, 2022'}
            description={'test text'}
            title={'title1'}
          />
        </SwiperSlide>
      </Swiper>
    </section>
  )
}
