import React from 'react'
import { TextField } from '@material-ui/core'
import { useFormContext } from 'react-hook-form'

interface FormFieldProps {
  name: string
  label: string
  rows: number
}

export const FormField: React.FC<FormFieldProps> = ({ name, label, rows }) => {
  const { register, formState } = useFormContext()

  return (
    <TextField
      {...register(name)}
      name={name}
      className="mb-20"
      //size="small"
      label={label}
      rows={rows}
      variant="outlined"
      error={!!formState.errors[name]?.message}
      helperText={formState.errors[name]?.message}
      fullWidth
    />
  )
}
