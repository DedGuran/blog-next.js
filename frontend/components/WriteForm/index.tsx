import React, { useEffect, useState } from 'react'
import { Button, Input, InputLabel, MenuItem } from '@material-ui/core'
import { Select } from '@mui/material'
import FormControl from '@mui/material/FormControl'
import styles from './WriteForm.module.scss'
import dynamic from 'next/dynamic'
import { Api } from '../../utils/api'
import { PostItem, ResponseCategory } from '../../utils/api/types'
import { useRouter } from 'next/router'
import { AddCategoryForm } from '../AddCategoryForm'
import { RemoveCategoryForm } from '../RemoveCategoryForm'
import 'react-quill/dist/quill.snow.css'

const Editor = dynamic(() => import('../Editor').then((m) => m.Editor), {
  ssr: false,
})
//const QuillEditor = dynamic(() => import('react-quill'), { ssr: false })

interface WriteFormProps {
  data?: PostItem
  categoryData?: ResponseCategory[]
}

export const WriteForm: React.FC<WriteFormProps> = ({ data, categoryData }) => {
  console.log('data', data)

  const router = useRouter()
  const quillModules = {
    toolbar: [
      [{ header: [1, 2, 3, false] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [{ list: 'ordered' }, { list: 'bullet' }],
      ['link', 'image'],
      [{ align: [] }],
      [{ color: [] }],
      ['code-block'],
      ['clean'],
    ],
  }
  const quillFormats = [
    'header',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'list',
    'bullet',
    'link',
    'image',
    'align',
    'color',
    'code-block',
  ]
  const [isLoading, setIsLoading] = useState(false)

  const [title, setTitle] = useState(data?.title || '')
  const [subtitle, setSubTitle] = useState(data?.title || '')
  const [keywords, setKeyWords] = useState(data?.keywords || '')
  const [tags, setTags] = useState(data?.tags || '')
  const [category, setCategory] = useState(data?.category || '')
  const [blocks, setBlocks] = useState(data?.body || [])

  const onAddPost = async () => {
    try {
      setIsLoading(true)
      const obj = {
        title,
        subtitle,
        keywords,
        tags,
        category,
        body: blocks,
      }
      if (!data) {
        const post = await Api().post.create(obj)

        await router.push(`/write/${post.id}`)
      } else {
        await Api().post.update(data.id, obj)
      }
    } catch (err) {
      console.warn(err)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <div className="xl:container md:px-20 py-16 text-center">
      <div className="flex flex-col mb-6">
        <div className="w-1/2 flex mb-10">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Заголовок
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              placeholder="Заголовок"
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
            />
          </div>
        </div>
        <div className="w-1/2 flex mb-10">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Подзаголовок
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              value={subtitle}
              onChange={(e) => setSubTitle(e.target.value)}
              placeholder="Подзаголовок"
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
            />
          </div>
        </div>
        <div className="w-1/2 flex mb-10">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Ключевые слова
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              value={keywords}
              onChange={(e) => setKeyWords(e.target.value)}
              placeholder="Ключевые слова"
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
            />
          </div>
        </div>

        <div className="w-1/2 flex mb-10">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4">
              Теги
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              value={tags}
              onChange={(e) => setTags(e.target.value)}
              placeholder="Теги"
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
            />
          </div>
        </div>

        <div className="flex item-center ml-10">
          {/* <form>
            <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
              Категории
            </label>
            <select
              onChange={(e) => setCategory(e.target.value)}
              value={1}
              id="countries"
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            >
              {categoryData?.map((obj) => (
                <option key={obj.id} value={obj.id}>
                  {obj.category}
                </option>
              ))}
            </select>
          </form> */}
          <FormControl sx={{ m: 1, minWidth: 120 }}>
            <InputLabel id="demo-simple-select-label">Категории</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={category}
              label="Category"
              onChange={(e) => setCategory(e.target.value)}
            >
              {categoryData?.map((obj) => (
                <MenuItem value={obj.id}>{obj.category}</MenuItem>
              ))}
            </Select>
          </FormControl>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <AddCategoryForm />
            <RemoveCategoryForm categoryData={categoryData} />
          </div>
        </div>
        {/* <div className="h-full">
          <QuillEditor
            value={data?.body}
            onChange={(arr) => setBlocks(arr)}
            modules={quillModules}
            formats={quillFormats}
            className="w-full h-[70%] mt-10 bg-white"
          />
        </div> */}
      </div>

      <div className="h-auto text-[18px]">
        <Editor
          initialBlocks={data?.body}
          onChange={(arr) => setBlocks(arr)}
          type={data ? 'edit' : 'add'}
        />
      </div>

      <div></div>
      <Button
        className={styles.addPostButton}
        disabled={isLoading || !blocks.length || !title}
        onClick={onAddPost}
        variant="contained"
        color="primary"
      >
        {data ? 'Сохранить' : 'Опубликовать'}
      </Button>
    </div>
  )
}
