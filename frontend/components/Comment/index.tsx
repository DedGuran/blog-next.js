import React, { useEffect, useState } from 'react'
import {
  Typography,
  IconButton,
  MenuItem,
  Menu,
  Avatar,
} from '@material-ui/core'
import MoreIcon from '@material-ui/icons/MoreHorizOutlined'

import styles from './Comment.module.scss'
import { CommentLikeItem, ResponseUser } from '../../utils/api/types'
import { Api } from '../../utils/api'
import { useDispatch } from 'react-redux'
import {
  fetchAddLike,
  fetchDeleteLike,
  fetchDeleteComment,
} from '../../redux/slices/comments'

interface CommentPostProps {
  id: number
  user: ResponseUser
  text: string
  createdAt: string
  currentUserId: number
  likes: CommentLikeItem[]
}

export const Comment: React.FC<CommentPostProps> = ({
  id,
  user,
  text,
  createdAt,
  currentUserId,
  likes,
}) => {
  const dispatch = useDispatch()
  const [anchorEl, setAnchorEl] = useState(null)
  const [activeLike, setActiveLike] = useState(false)
  const [likesData, setLikesData] = useState([])

  const AddLike = () => {
    const obj = {
      userId: currentUserId,
      commentId: id,
    }
    try {
      dispatch(fetchAddLike(obj))
      setActiveLike(true)
    } catch (err) {
      console.warn('Error for add like', err)
    }
  }
  const DeleteLike = () => {
    const foundLike = likesData?.find((like) => like.userId === user.id)

    try {
      dispatch(fetchDeleteLike(foundLike.id))
      setActiveLike(false)
    } catch (err) {
      console.warn('Error for delite like', err)
    }
  }
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  const handleClickRemove = async () => {
    if (window.confirm('Удалить комментарий?')) {
      try {
        dispatch(fetchDeleteComment(id))
        //await Api().comment.remove(id)
        //onRemove(id)
      } catch (err) {
        console.warn('Error remove comment', err)
      } finally {
        handleClose()
      }
    }
  }
  useEffect(() => {
    setLikesData(likes)
    likes.forEach((like) => {
      if (like.userId == user.id) {
        setActiveLike(true)
      }
    })
  }, [likes])

  return (
    <div className={styles.comment}>
      <div className={styles.userInfo}>
        <Avatar style={{ marginRight: 10 }}>{user?.fullName[0]}</Avatar>
        <b>{user?.fullName}</b>
        <span>
          {new Date(createdAt).toLocaleDateString('ru-RU', {
            month: '2-digit',
            day: '2-digit',
            year: 'numeric',
          })}
        </span>
      </div>
      <Typography className={styles.text}>{text}</Typography>
      <>
        {activeLike ? (
          <span
            className="flex mr-10 cursor-pointer text-sm"
            onClick={DeleteLike}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20px"
              height="20px"
              viewBox="0 0 24 24"
              fill="#000"
            >
              <path
                d="M8 10V20M8 10L4 9.99998V20L8 20M8 10L13.1956 3.93847C13.6886 3.3633 14.4642 3.11604 15.1992 3.29977L15.2467 3.31166C16.5885 3.64711 17.1929 5.21057 16.4258 6.36135L14 9.99998H18.5604C19.8225 9.99998 20.7691 11.1546 20.5216 12.3922L19.3216 18.3922C19.1346 19.3271 18.3138 20 17.3604 20L8 20"
                stroke="#000000"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <p className="ml-10">{likesData?.length}</p>
          </span>
        ) : (
          <span onClick={AddLike} className="flex mr-10 cursor-pointer text-sm">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="20px"
              height="20px"
              viewBox="0 0 24 24"
              fill="none"
            >
              <path
                d="M8 10V20M8 10L4 9.99998V20L8 20M8 10L13.1956 3.93847C13.6886 3.3633 14.4642 3.11604 15.1992 3.29977L15.2467 3.31166C16.5885 3.64711 17.1929 5.21057 16.4258 6.36135L14 9.99998H18.5604C19.8225 9.99998 20.7691 11.1546 20.5216 12.3922L19.3216 18.3922C19.1346 19.3271 18.3138 20 17.3604 20L8 20"
                stroke="#000000"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            <p className="ml-10">{likesData?.length}</p>
          </span>
        )}
      </>
      {user?.id === currentUserId && (
        <>
          <span className={styles.replyBtn}>Ответить</span>
          <IconButton onClick={handleClick}>
            <MoreIcon />
          </IconButton>
          <Menu
            anchorEl={anchorEl}
            elevation={2}
            open={Boolean(anchorEl)}
            onClose={handleClose}
            keepMounted
          >
            <MenuItem onClick={handleClickRemove}>Удалить</MenuItem>
            <MenuItem onClick={handleClose}>Редактировать</MenuItem>
          </Menu>
        </>
      )}
    </div>
  )
}
