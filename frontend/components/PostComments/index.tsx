import React, { useEffect, useState } from 'react'
import { Divider, Paper, Tab, Tabs, Typography } from '@material-ui/core'
import { Comment } from '../Comment'
import { AddCommentForm } from '../AddCommentForm'
import { CommentItem } from '../../utils/api/types'
import { useAppSelector } from '../../redux/hooks'
import { selectUserData } from '../../redux/slices/users'
import { useComments } from '../../hooks/useComments'
import { useDispatch, useSelector } from 'react-redux'
import { fetchCommentsByPostId } from '../../redux/slices/comments'
import styles from './PostComments.module.scss'

interface PostComments {
  postId: number
}
export const PostComments: React.FC<PostComments> = ({ postId }) => {
  const dispatch = useDispatch()
  const data = useSelector((state) => state.comments)

  const userData = useAppSelector(selectUserData)
  const [activeTab, setActiveTab] = useState(0)
  //const { comments, setComments } = useComments(postId)

  const isCommentLoading = data.comments.status === 'loaded'
  useEffect(() => {
    dispatch(fetchCommentsByPostId(postId))
  }, [postId])
  useEffect(() => {
    console.log('new comments', data.comments.items)
  }, [data.comments.items])
  return (
    <Paper
      elevation={0}
      className="mt-40 p-30 "
      classes={{ root: styles.paper }}
    >
      <div className="container">
        <Typography variant="h6" className="mb-20">
          {`${data.comments.items?.filter((obj) => obj.active == true).length} комментария`}
        </Typography>
        <Tabs
          onChange={(_, newValue) => setActiveTab(newValue)}
          className="mt-20"
          value={activeTab}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="Популярные" />
          <Tab label="По порядку" />
        </Tabs>
        <Divider />
        {isCommentLoading && <AddCommentForm postId={postId} />}

        {isCommentLoading ? (
          data.comments.items
            ?.filter((obj) => obj.active == true)
            .map((obj) => (
              <Comment
                key={obj.id}
                id={obj.id}
                user={obj.user}
                text={obj.text}
                likes={obj.likes}
                createdAt={obj.createdAt}
                currentUserId={userData?.id}
              />
            ))
        ) : (
          <div>Загрузка</div>
        )}
      </div>
    </Paper>
  )
}
