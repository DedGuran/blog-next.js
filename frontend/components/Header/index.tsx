import React, { useState, useEffect } from 'react'
import Link from 'next/link'
import {
  Paper,
  Button,
  IconButton,
  Avatar,
  List,
  ListItem,
} from '@material-ui/core'

import {
  SearchOutlined as SearchIcon,
  SmsOutlined as MessageIcon,
  Menu as MenuIcon,
  ExpandMoreOutlined as ArrowBottom,
  NotificationsNoneOutlined as NotificationIcon,
  AccountCircleOutlined as UserIcon,
} from '@material-ui/icons'

import { fetchGetMe } from '../../redux/slices/users'
import { PostItem } from '../../utils/api/types'
import { Api } from '../../utils/api'
import { HeaderDropdown } from '../HeaderDropdown'
import { signIn } from 'next-auth/react'
import { useDispatch, useSelector } from 'react-redux'

export const Header: React.FC = () => {
  const dispatch = useDispatch()
  const userData = useSelector((state) => state.user.user.item)
  //const dataSession = useSession()
  const [authVisible, setAuthVisible] = useState(false)
  const [searchValue, setSearchValue] = useState('')
  const [posts, setPosts] = useState<PostItem[]>([])

  const openAuthDialog = () => {
    setAuthVisible(true)
  }

  const closeAuthDialog = () => {
    setAuthVisible(false)
  }

  const handleChangeInput = async (e) => {
    setSearchValue(e.target.value)
    try {
      const { items } = await Api().post.search({ title: e.target.value })
      setPosts(items)
    } catch (err) {
      console.warn(err)
    }
  }
  useEffect(() => {
    dispatch(fetchGetMe())
  }, [])

  return (
    <header className="bg-gray-50">
      <div className="xl:container xl:mx-auto flex flex-col items-center sm:flex-row sm:justify-between text-center py-3">
        <div className="md:flex-none w-96 order-2 sm:order-1 flex justify-center py-4 sm:py-0 ">
          <input
            value={searchValue}
            onChange={handleChangeInput}
            placeholder="Поиск"
            className="mt-1 block w-60 px-3 py-2 bg-white border-slate-300 rounded-full text-sm shadow-sm placeholder-slate-500 focus:outline-none focus:border-sky-500 focus:ring-1 focus:ring-sky-500"
          />
          {posts.length > 0 && searchValue && (
            // <Paper className={styles.searchBlockPopup}>
            <Paper className="absolute top-20 mr-50">
              <List>
                {posts.map((obj) => (
                  <Link key={obj.id} href={`/post/${obj.id}`}>
                    <ListItem key={obj.id} button>
                      {obj.title}
                    </ListItem>
                  </Link>
                ))}
              </List>
            </Paper>
          )}

          {userData?.role == 'admin' ? (
            <Link href="/write">
              <button className="bg-orange-400 px-5 py-3 rounded-full text-gray-50 text-xl mx-10">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  aria-label="Write"
                >
                  <path
                    d="M14 4a.5.5 0 0 0 0-1v1zm7 6a.5.5 0 0 0-1 0h1zm-7-7H4v1h10V3zM3 4v16h1V4H3zm1 17h16v-1H4v1zm17-1V10h-1v10h1zm-1 1a1 1 0 0 0 1-1h-1v1zM3 20a1 1 0 0 0 1 1v-1H3zM4 3a1 1 0 0 0-1 1h1V3z"
                    fill="currentColor"
                  ></path>
                  <path
                    d="M17.5 4.5l-8.46 8.46a.25.25 0 0 0-.06.1l-.82 2.47c-.07.2.12.38.31.31l2.47-.82a.25.25 0 0 0 .1-.06L19.5 6.5m-2-2l2.32-2.32c.1-.1.26-.1.36 0l1.64 1.64c.1.1.1.26 0 .36L19.5 6.5m-2-2l2 2"
                    stroke="currentColor"
                  ></path>
                </svg>
              </button>
            </Link>
          ) : null}
        </div>

        <div className="shrink w-80 sm:order-2">
          <Link className="font-bold uppercase text-3xl" href={'/'}>
            Veda Info
          </Link>
        </div>
        <div className="w-96 order-3 flex justify-center">
          {userData ? (
            <Avatar
              className=""
              alt="Remy Sharp"
              onClick={openAuthDialog}
              src={userData.avatar ? userData.avatar : ''}
            />
          ) : (
            <div
              //className={styles.loginButton}
              className=""
              onClick={() => {
                signIn()
              }}
            >
              <UserIcon />
              Войти
            </div>
          )}
        </div>
        <HeaderDropdown
          visible={authVisible}
          onClose={closeAuthDialog}
          userInfo={userData}
        />
      </div>
    </header>
  )
}
function dispatch(arg0: any) {
  throw new Error('Function not implemented.')
}
