import React, { useState } from 'react'
import Link from 'next/link'
import clsx from 'clsx'
import { CardContent, Paper, Typography } from '@material-ui/core'
import DeleteIcon from '@mui/icons-material/Clear'
import EditIcon from '@mui/icons-material/Edit'
import IconButton from '@mui/material/IconButton'

import styles from './Post.module.scss'
import { PostActions } from '../PostActions/PostActions'
import { Api } from '../../utils/api'
import { useRouter } from 'next/router'
import CardMedia from '@mui/material/CardMedia'
import { useComments } from '../../hooks/useComments'

interface PostProps {
  title: string
  id: number
  description: string
  subtitle: string
  imageUrl?: string
  isEditable: boolean
  category?: string
  views: number
}

export const Post: React.FC<PostProps> = ({
  title,
  subtitle,
  description,
  category,
  id,
  imageUrl,
  isEditable,
  views,
}) => {
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(false)
  const { comments, setComments } = useComments(id)
  const onClickRemove = async (id) => {
    if (window.confirm('Вы действительно хотите удалить стать?')) {
      try {
        setIsLoading(true)
        const post = await Api().post.remove(id)
        await router.push(`/`)
      } catch (err) {
        console.warn(err)
      } finally {
        setIsLoading(false)
      }
    }
  }

  return (
    <Paper elevation={0} className="p-20" classes={{ root: styles.paper }}>
      {isEditable && (
        <div className={styles.editButtons}>
          <Link href={`/write/${id}`}>
            <IconButton color="primary">
              <EditIcon />
            </IconButton>
          </Link>
          <IconButton
            onClick={() => {
              onClickRemove(id)
            }}
            color="secondary"
          >
            <DeleteIcon />
          </IconButton>
        </div>
      )}
      {imageUrl && (
        <img
          src={imageUrl}
          alt={title}
          style={{ height: '20vh', width: '100vh' }}
        />
      )}
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          <Link href={`/post/${id}`}>{title}</Link>
        </Typography>

        <Typography className={styles.category}>
          <Link href={`/category/${id}`}>{category}</Link>
        </Typography>
        <Typography
          variant="body2"
          color="textSecondary"
          component="p"
          className={styles.description}
        >
          {subtitle}
        </Typography>
      </CardContent>
      <PostActions views={views} commentsLength={comments.length} />
    </Paper>
  )
}
function setIsLoading(arg0: boolean) {
  throw new Error('Function not implemented.')
}
