import React, { useEffect, useState } from 'react'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { HomeSlider } from './_childComponent/HomeSlider'
import SwiperCore, { Autoplay } from 'swiper'
import { selectUserData } from '../redux/slices/users'
import { useAppSelector } from '../redux/hooks'
export default function Section1(posts) {
  const userData = useAppSelector(selectUserData)
  SwiperCore.use([Autoplay])

  return (
    <section className="pt-16 pb-2">
      <div className="content mx-auto md:px-20">
        <Swiper
          className="xl:container"
          slidesPerView={1}
          autoplay={{ delay: 5000 }}
        >
          {Array.isArray(posts.postsData) &&
            posts.postsData?.map((obj) => (
              <SwiperSlide key={obj.id}>
                <HomeSlider
                  key={obj.id}
                  userRole={obj.user.role}
                  categoryId={obj.category.id}
                  subtitle={obj.subtitle}
                  id={obj.id}
                  title={obj.title}
                  category={obj.category?.category}
                  description={obj.description}
                  image={
                    obj.body.find((obj) => obj.type === 'image')?.data?.file.url
                  }
                  date={obj.createdAt}
                  isEditable={userData?.id === obj.user.id}
                  userName={obj.user.fullName}
                  userFoto={obj.user.avatar}
                  userStatus={obj.user.status}
                />
              </SwiperSlide>
            ))}
        </Swiper>
      </div>
    </section>
  )
}
