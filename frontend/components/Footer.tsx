import React, { useState, useEffect } from 'react'

export const Footer: React.FC = () => {
  return (
    <footer className="bg-gray-50">
      <section className="bg-gray-50 mt-20">
        <div className="mx-auto md:px-20 py-16 text-center">
          <h1 className="font-bold text-3xl">Подпишитесь на рассылку</h1>
          <div className="py-4">
            <input
              type="text"
              className="px-4 shadow border rounded w-9/12 py-3 text-gray-700 focus:outline-none focus:shadow-outline"
              placeholder="Введите ваш email"
            />
          </div>
          <button className="bg-orange-400 px-20 py-3 rounded-full text-gray-50 text-xl">
            Подписаться
          </button>
        </div>
      </section>
      <div className="container mx-auto flex justify-center py-12">
        <div className="py-5">
          <div className="flex gap-6 justify-center"></div>
          <p className="py-5 text-gray-400">
            Copyright 2023 All rights reserved | This template is made with by
            Gurlev Andrey
          </p>
          <p className="text-gray-400 text-center">Terms & Condition</p>
        </div>
      </div>
    </footer>
  )
}
