import { Api } from '../utils/api'

export default async (email: 'string', password: 'string', image: 'string') => {
  try {
    const registerUser = await Api().user.register({
      fullName: email,
      email: email,
      password: password,
      avatar: image,
      role: 'user',
    })

    if (registerUser) {
      return registerUser
    } else {
      return null
    }
  } catch (err) {
    try {
      const loginUser = await Api().user.login({
        email: email,
        password: password,
      })

      if (loginUser) {
        return loginUser
      } else {
        return null
      }
    } catch (error) {
      // console.log(error)
    }
  }
}
