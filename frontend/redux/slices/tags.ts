import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'
import { ResponseTags } from '../../utils/api/types'
import { axiosInstance } from '../../axios/axios'

const axios = axiosInstance()
export interface TagsState {
  tags: {
    item: ResponseTags[] | null
    status: 'loading' | 'loaded' | 'error' | null
  }
}
export const fetchGetAllTags = createAsyncThunk(
  '/post/fetchGetAllTags',
  async () => {
    try {
      const { data } = await axios.get(`/posts/tags`)

      return data
    } catch (err) {
      console.warn('Не удалось получить теги', err)
    }
  }
)

const initialState: TagsState = {
  tags: {
    item: null,
    status: 'loading',
  },
}
export const tagsSlice = createSlice({
  name: 'tags',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchGetAllTags.pending, (state) => {
        state.tags.status = 'loading'
      })
      .addCase(fetchGetAllTags.fulfilled, (state, { payload }) => {
        state.tags.status = 'loaded'
        state.tags.item = payload
      })
      .addCase(fetchGetAllTags.rejected, (state) => {
        state.tags.status = 'error'
      })
  },
})

export const tagsReducer = tagsSlice.reducer
