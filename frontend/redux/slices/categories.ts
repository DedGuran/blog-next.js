import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'
import { ResponseCategory } from '../../utils/api/types'
import { axiosInstance } from '../../axios/axios'

const axios = axiosInstance()
export interface CategoriesState {
  categories: {
    item: ResponseCategory[] | null
    status: 'loading' | 'loaded' | 'error' | null
  }
}
export const fetchGetAllCategories = createAsyncThunk(
  '/post/fetchGetAllCategories',
  async () => {
    try {
      const { data } = await axios.get(`/category`)

      return data
    } catch (err) {
      console.warn('Не удалось получить категории', err)
    }
  }
)

const initialState: CategoriesState = {
  categories: {
    item: null,
    status: 'loading',
  },
}
export const categoriesSlice = createSlice({
  name: 'categories',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchGetAllCategories.pending, (state) => {
        state.categories.status = 'loading'
      })
      .addCase(fetchGetAllCategories.fulfilled, (state, { payload }) => {
        state.categories.status = 'loaded'
        state.categories.item = payload
      })
      .addCase(fetchGetAllCategories.rejected, (state) => {
        state.categories.status = 'error'
      })
  },
})

export const categoriesReducer = categoriesSlice.reducer
