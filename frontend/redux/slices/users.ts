import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'
import { ResponseUser } from '../../utils/api/types'
import { AppState } from '../store'
import { axiosInstance } from '../../axios/axios'
import { Api } from '../../utils/api'
const axios = axiosInstance()
export interface UserState {
  user: {
    item: ResponseUser | null
    status: 'loading' | 'loaded' | 'error' | null
  }
  users: {
    item: ResponseUser[] | null
    status: 'loading' | 'loaded' | 'error' | null
  }
}
export const fetchGetMe = createAsyncThunk('/users/fetchGetMe', async (ctx) => {
  try {
    const { data } = await axiosInstance(ctx).get(`/users/me`)

    return data
  } catch (err) {
    console.warn('Не удалось получить данные пользователя', err)
  }
})
export const fetchGetAllUsers = createAsyncThunk(
  '/users/fetchGetAllUsers',
  async (ctx) => {
    try {
      const { data } = await axiosInstance(ctx).get(`/users`)

      return data
    } catch (err) {
      console.warn('Не удалось получить данные всех пользователей', err)
    }
  }
)
export const fetchUpdateMe = createAsyncThunk(
  '/users/fetchUpdateMe',
  async (dto) => {
    try {
      const { data } = await axios.patch(`/users/me`, dto)

      return data
    } catch (err) {
      console.warn('Не удалось обновить данные пользователя', err)
    }
  }
)
export const fetchRegistration = createAsyncThunk(
  '/users/fetchRegistration',
  async (dto) => {
    try {
      const { data } = await axios.post(`/auth/register`, dto)

      return data
    } catch (err) {
      console.warn('Не удалось зарегестрировать пользователя', err)
    }
  }
)
export const fetchLogin = createAsyncThunk('/users/fetchLogin', async (dto) => {
  try {
    const { data } = await axios.post(`/auth/login`, dto)

    return data
  } catch (err) {
    console.warn('Не удалось залогинить пользователя', err)
  }
})

export const fetchUserByEmail = createAsyncThunk(
  '/users/fetchUserByEmail',
  async (email) => {
    try {
      const { data } = await axios.get(`/users/me/${email}`)

      return data
    } catch (err) {
      console.warn('Не удалось найти пользователя по email', err)
    }
  }
)
const initialState: UserState = {
  user: {
    item: null,
    status: 'loading',
  },
  users: {
    item: null,
    status: 'loading',
  },
}
export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUserData: (state, action: PayloadAction<ResponseUser>) => {
      state.user.item = action.payload
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchGetMe.pending, (state) => {
        state.user.status = 'loading'
      })
      .addCase(fetchGetMe.fulfilled, (state, { payload }) => {
        state.user.status = 'loaded'
        state.user.item = payload
      })
      .addCase(fetchGetMe.rejected, (state) => {
        state.user.status = 'error'
      })
      .addCase(fetchGetAllUsers.pending, (state) => {
        state.users.status = 'loading'
      })
      .addCase(fetchGetAllUsers.fulfilled, (state, { payload }) => {
        state.users.status = 'loaded'
        state.users.item = payload
      })
      .addCase(fetchGetAllUsers.rejected, (state) => {
        state.users.status = 'error'
      })
      .addCase(fetchUpdateMe.pending, (state) => {
        state.user.status = 'loading'
      })
      .addCase(fetchUpdateMe.fulfilled, (state, { payload }) => {
        state.user.status = 'loaded'
        state.user.item = payload
      })
      .addCase(fetchUpdateMe.rejected, (state) => {
        state.user.status = 'error'
      })
  },
})

export const { setUserData } = userSlice.actions
export const selectUserData = (state: AppState) => state.user.user.item

export const userReducer = userSlice.reducer
