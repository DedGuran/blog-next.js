import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'
import { PostItem, ResponseUser } from '../../utils/api/types'
import { AppState } from '../store'
import { axiosInstance } from '../../axios/axios'
import { Api } from '../../utils/api'
const axios = axiosInstance()
export interface PostState {
  posts: {
    item: PostItem[] | null
    status: 'loading' | 'loaded' | 'error' | null
  }
  post: {
    item: PostItem | null
    status: 'loading' | 'loaded' | 'error' | null
  }
  popularPosts: {
    item: PostItem[] | null
    status: 'loading' | 'loaded' | 'error' | null
  }
  postsByUserId: {
    item: PostItem[] | null
    status: 'loading' | 'loaded' | 'error' | null
  }
}
export const fetchGetAllPosts = createAsyncThunk(
  '/post/fetchGetAllPosts',
  async () => {
    try {
      const { data } = await axios.get(`/posts`)

      return data
    } catch (err) {
      console.warn('Не удалось получить посты', err)
    }
  }
)
export const fetchGetOnePost = createAsyncThunk(
  '/post/fetchGetOnePost',
  async (id) => {
    try {
      const { data } = await axios.get(`/posts/${id}`)

      return data
    } catch (err) {
      console.warn('Не удалось получить пост', err)
    }
  }
)

export const fetchGetPopularPosts = createAsyncThunk(
  '/post/fetchGetPopularPosts',
  async () => {
    try {
      const { data } = await axios.get(`/posts/popular`)

      return data
    } catch (err) {
      console.warn('Не удалось получить популярные посты', err)
    }
  }
)
export const fetchGetPostsByUserId = createAsyncThunk(
  '/post/fetchGetPostsByUserId',
  async (id) => {
    try {
      const { data } = await axios.get(`/posts/user/${id}`)

      return data
    } catch (err) {
      console.warn('Не удалось получить посты по id пользователя', err)
    }
  }
)
const initialState: PostState = {
  posts: {
    item: null,
    status: 'loading',
  },
  post: {
    item: null,
    status: 'loading',
  },
  popularPosts: {
    item: null,
    status: 'loading',
  },
  postsByUserId: {
    item: null,
    status: 'loading',
  },
}
export const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchGetAllPosts.pending, (state) => {
        state.posts.status = 'loading'
      })
      .addCase(fetchGetAllPosts.fulfilled, (state, { payload }) => {
        state.posts.status = 'loaded'
        state.posts.item = payload
      })
      .addCase(fetchGetAllPosts.rejected, (state) => {
        state.posts.status = 'error'
      })

      .addCase(fetchGetOnePost.pending, (state) => {
        state.post.status = 'loading'
      })
      .addCase(fetchGetOnePost.fulfilled, (state, { payload }) => {
        state.post.status = 'loaded'
        state.post.item = payload
      })
      .addCase(fetchGetOnePost.rejected, (state) => {
        state.post.status = 'error'
      })

      .addCase(fetchGetPopularPosts.pending, (state) => {
        state.popularPosts.status = 'loading'
      })
      .addCase(fetchGetPopularPosts.fulfilled, (state, { payload }) => {
        state.popularPosts.status = 'loaded'
        state.popularPosts.item = payload
      })
      .addCase(fetchGetPopularPosts.rejected, (state) => {
        state.popularPosts.status = 'error'
      })
      .addCase(fetchGetPostsByUserId.pending, (state) => {
        state.postsByUserId.status = 'loading'
      })
      .addCase(fetchGetPostsByUserId.fulfilled, (state, { payload }) => {
        state.postsByUserId.status = 'loaded'
        state.postsByUserId.item = payload
      })
      .addCase(fetchGetPostsByUserId.rejected, (state) => {
        state.postsByUserId.status = 'error'
      })
  },
})

export const postsReducer = postsSlice.reducer
