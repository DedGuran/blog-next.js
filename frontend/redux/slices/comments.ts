import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { CommentItem } from '../../utils/api/types'
import { AppState } from '../store'
import { axiosInstance } from '../../axios/axios'
const axios = axiosInstance()

export interface CommentsState {
  comments: {
    items: CommentItem[] | null
    status: 'loading' | 'loaded' | 'error' | null
  }
}
export const fetchGetAllComments = createAsyncThunk(
  '/comments/fetchGetAllComments',
  async () => {
    try {
      const { data } = await axios.get(`comments/`)
      return data
    } catch (err) {
      console.warn('Ошибка взятием всех комментариев', err)
    }
  }
)
export const fetchCommentsByPostId = createAsyncThunk(
  '/comments/fetchCommentsByPostId',
  async (postId) => {
    try {
      const { data } = await axios.get(`comments/postId/${postId}`)
      return data
    } catch (err) {
      console.warn('Ошибка взятием комментариев по id', err)
    }
  }
)
export const fetchAddComment = createAsyncThunk(
  'comments/fetchAddComment',
  async (dto) => {
    try {
      const { data } = await axios.post(`comments/`, dto)
      return data
    } catch (err) {
      console.warn('Ошибка с созданием комментария', err)
    }
  }
)
export const fetchDeleteComment = createAsyncThunk(
  '/comments/fetchDeleteComment',
  async (id) => {
    try {
      const { data } = await axios.delete(`comments/${id}`)
      return data
    } catch (err) {
      console.warn('err', err)
    }
  }
)
export const fetchAddLike = createAsyncThunk(
  'comments/fetchAddLike',
  async (dto) => {
    try {
      const { data } = await axios.post(`commentlike/`, dto)
      return data
    } catch (err) {
      console.warn('Ошибка с добавлением лайка в комментарий', err)
    }
  }
)
export const fetchDeleteLike = createAsyncThunk(
  'comments/fetchDeleteLike',
  async (id) => {
    try {
      const { data } = await axios.delete(`commentlike/${id}`)
      return data
    } catch (err) {
      console.warn('Ошибка с удалением лайка для комментария', err)
    }
  }
)
const initialState: CommentsState = {
  comments: {
    items: [],
    status: 'loading',
  },
}
export const commentsSlice = createSlice({
  name: 'comments',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchGetAllComments.pending, (state) => {
        state.comments.status = 'loading'
      })
      .addCase(fetchGetAllComments.fulfilled, (state, { payload }) => {
        state.comments.status = 'loaded'
        state.comments.items = payload
      })
      .addCase(fetchGetAllComments.rejected, (state) => {
        state.comments.status = 'error'
      })
      .addCase(fetchAddComment.pending, (state) => {
        state.comments.status = 'loading'
      })
      .addCase(fetchAddComment.fulfilled, (state, { payload }) => {
        state.comments.status = 'loaded'
        state.comments.items = [...state.comments.items, payload[0]]
      })
      .addCase(fetchAddComment.rejected, (state) => {
        state.comments.status = 'error'
      })
      .addCase(fetchCommentsByPostId.pending, (state) => {
        state.comments.status = 'loading'
      })
      .addCase(fetchCommentsByPostId.fulfilled, (state, { payload }) => {
        state.comments.status = 'loaded'
        state.comments.items = payload
      })
      .addCase(fetchCommentsByPostId.rejected, (state) => {
        state.comments.status = 'error'
      })
      .addCase(fetchDeleteComment.pending, (state) => {
        state.comments.status = 'loading'
      })
      .addCase(fetchDeleteComment.fulfilled, (state, { payload }) => {
        state.comments.status = 'loaded'
        state.comments.items = state.comments.items.filter(
          (obj) => obj.id !== payload.id
        )
      })
      .addCase(fetchDeleteComment.rejected, (state) => {
        state.comments.status = 'error'
      })
      .addCase(fetchAddLike.pending, (state) => {
        state.comments.status = 'loading'
      })
      .addCase(fetchAddLike.fulfilled, (state, { payload }) => {
        const { userId, comment, id } = payload
        const commentIndex = state.comments.items.findIndex(
          (item) => item.id === comment.id
        )
        if (commentIndex !== -1) {
          state.comments.items[commentIndex].likes.push({ id, userId })
        }
      })
      .addCase(fetchAddLike.rejected, (state) => {
        state.comments.status = 'error'
      })
      .addCase(fetchDeleteLike.pending, (state) => {
        state.comments.status = 'loading'
      })
      .addCase(fetchDeleteLike.fulfilled, (state, { payload }) => {
        state.comments.status = 'loaded'
        state.comments.items.forEach((comment) => {
          comment.likes = comment.likes.filter((obj) => obj.id !== payload.id)
        })
      })
      .addCase(fetchDeleteLike.rejected, (state) => {
        state.comments.status = 'error'
      })
  },
})

//export const { setCommentsData } = commentsSlice.actions
export const selectCommentsData = (state: AppState) => state.comments

export const commentsReducer = commentsSlice.reducer
