import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit'
import { HYDRATE } from 'next-redux-wrapper'
import { FavoritesItem } from '../../utils/api/types'
import { AppState } from '../store'
import { axiosInstance } from '../../axios/axios'
const axios = axiosInstance()

export interface FavoritesState {
  favorites: {
    items: FavoritesItem[] | null
    status: 'loading' | 'loaded' | 'error' | null
  }
}
export const fetchGetAllFavorites = createAsyncThunk(
  '/favoriteposts/fetchGetAllFavorites',
  async () => {
    try {
      const { data } = await axios.get(`favoriteposts/`)
      return data
    } catch (err) {
      console.warn('Ошибка взятием всех любимых постов', err)
    }
  }
)

export const fetchAddFavorites = createAsyncThunk(
  '/favoriteposts/fetchAddFavorites',
  async (dto) => {
    try {
      const { data } = await axios.post(`favoriteposts/`, dto)
      return data
    } catch (err) {
      console.warn('Ошибка с добавлением в избранное', err)
    }
  }
)
export const fetchDeleteFavorites = createAsyncThunk(
  '/favoriteposts/fetchDeleteFavorites',
  async (id) => {
    try {
      const { data } = await axios.delete(`favoriteposts/${id}`)
      return data
    } catch (err) {
      console.warn('err', err)
    }
  }
)

const initialState: FavoritesState = {
  favorites: {
    items: [],
    status: 'loading',
  },
}
export const favoritesSlice = createSlice({
  name: 'favorites',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchGetAllFavorites.pending, (state) => {
        state.favorites.status = 'loading'
      })
      .addCase(fetchGetAllFavorites.fulfilled, (state, { payload }) => {
        state.favorites.status = 'loaded'
        state.favorites.items = payload
      })
      .addCase(fetchGetAllFavorites.rejected, (state) => {
        state.favorites.status = 'error'
      })
      .addCase(fetchAddFavorites.pending, (state) => {
        state.favorites.status = 'loading'
      })
      .addCase(fetchAddFavorites.fulfilled, (state, { payload }) => {
        state.favorites.status = 'loaded'
        state.favorites.items = [...state.favorites.items, payload]
        //console.log('test11e2',state.comments.items)
      })
      .addCase(fetchAddFavorites.rejected, (state) => {
        state.favorites.status = 'error'
      })

      .addCase(fetchDeleteFavorites.pending, (state) => {
        state.favorites.status = 'loading'
      })
      .addCase(fetchDeleteFavorites.fulfilled, (state, { payload }) => {
        state.favorites.status = 'loaded'
        state.favorites.items = state.favorites.items.filter(
          (obj) => obj.id !== payload.id
        )
      })
      .addCase(fetchDeleteFavorites.rejected, (state) => {
        state.favorites.status = 'error'
      })
  },
})

//export const { setCommentsData } = commentsSlice.actions
export const selectFavoritesData = (state: AppState) => state.favorites

export const favoritesReducer = favoritesSlice.reducer
