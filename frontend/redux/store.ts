import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import { userReducer } from './slices/users'
import { createWrapper } from 'next-redux-wrapper'
import { commentsReducer } from './slices/comments'
import { postsReducer } from './slices/posts'
import { categoriesReducer } from './slices/categories'
import { tagsReducer } from './slices/tags'
import { favoritesReducer } from './slices/favorites'

//import counterReducer from '../features/counter/counterSlice'

export function makeStore() {
  return configureStore({
    reducer: {
      user: userReducer,
      comments: commentsReducer,
      posts: postsReducer,
      categories: categoriesReducer,
      tags: tagsReducer,
      favorites: favoritesReducer,
    },
  })
}

export const store = makeStore()

export type AppStore = ReturnType<typeof makeStore>
export type AppState = ReturnType<AppStore['getState']>
export type AppDispatch = typeof store.dispatch

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>

export const wrapper = createWrapper<AppStore>(makeStore)
