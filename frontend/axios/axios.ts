import axios from 'axios'
import Cookies, { parseCookies } from 'nookies'
import { NextPageContext, GetServerSidePropsContext } from 'next'

export const axiosInstance = (
  ctx?: NextPageContext | GetServerSidePropsContext
) => {
  const cookies = ctx ? Cookies.get(ctx) : parseCookies()

  const token = cookies['token']

  const instance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_BACKEND_URL,
  })

  instance.interceptors.request.use((config) => {
    config.headers.Authorization = `Bearer ${token}`
    return config
  })

  return instance
}
