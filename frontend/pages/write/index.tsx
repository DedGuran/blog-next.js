import React, { useEffect } from 'react'
import { NextPage } from 'next'

import { MainLayout } from '../../layouts/MainLayout'
import { WriteForm } from '../../components/WriteForm'
import { Api } from '../../utils/api'
import { ResponseCategory } from '../../utils/api/types'

import { getSession } from 'next-auth/react'

interface WriteProps {
  categories: ResponseCategory[]
}
const WritePage: NextPage<WriteProps> = ({ categories }) => {
  return (
    <MainLayout className="main-layout-white" hideComments hideMenu>
      <WriteForm categoryData={categories} />
    </MainLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  const session = await getSession({ req: ctx.req })

  if (!session || session.user.role !== 'admin') {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  try {
    const categories = await Api().category.getAll()

    return {
      props: {
        categories: categories || [],
      },
    }
  } catch (err) {
    console.warn(err)
  }
  return {
    props: {
      categoryData: null,
    },
  }
}

export default WritePage
