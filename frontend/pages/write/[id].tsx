import React, { useEffect } from 'react'
import { GetServerSideProps, NextPage } from 'next'
import { MainLayout } from '../../layouts/MainLayout'
import { WriteForm } from '../../components/WriteForm'
import { Api } from '../../utils/api'
import { PostItem, ResponseCategory } from '../../utils/api/types'
import { useRouter } from 'next/router'
import { useAppSelector } from '../../redux/hooks'
import { selectUserData } from '../../redux/slices/users'
import { getSession } from 'next-auth/react'
interface WritePageProps {
  post: PostItem
  categoryData: ResponseCategory[]
}

const WritePage: NextPage<WritePageProps> = ({ post, categoryData }) => {
  return (
    <MainLayout className="main-layout-white" hideComments hideMenu>
      <WriteForm data={post} categoryData={categoryData} />
    </MainLayout>
  )
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const session = await getSession({ req: ctx.req })

  if (!session || session.user.role !== 'admin') {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  try {
    const id = ctx.params.id
    const post = await Api(ctx).post.getOne(+id)
    const user = await Api(ctx).user.getMe()
    const categoryData = await Api().category.getAll()
    if (post.user.id !== user.id) {
      return {
        props: {},
        redirect: {
          destination: '/',
          permanent: false,
        },
      }
    }
    return { props: { post, categoryData } }
  } catch (err) {
    return {
      props: {},
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
}
export default WritePage
