import React, { useEffect, useState } from 'react'
import { MainLayout } from '../layouts/MainLayout'
import { NextPage } from 'next'
import { PostItem, ResponseCategory } from '../utils/api/types'
import Head from 'next/head'
import Section1 from '../components/Section1'
import { Section2 } from '../components/Section2'
import Section4 from '../components/Section4'
import { fetchGetAllPosts } from '../redux/slices/posts'
import { store } from '../redux/store'
import { fetchGetAllCategories } from '../redux/slices/categories'
import { fetchGetAllTags } from '../redux/slices/tags'
import { Header } from '../components/Header'
import { Footer } from '../components/Footer'
import { useAppSelector } from '../redux/hooks'
import { selectUserData } from '../redux/slices/users'

interface HomeProps {
  posts: PostItem[]
  categories: ResponseCategory[]
  tags: any
}

const Home: NextPage<HomeProps> = ({ posts, categories, tags }) => {
  const [selectedCategoryId, setSelectedCategoryId] = useState(null)
  const [selectedTagsName, setSelectedTagsName] = useState([])
  const userData = useAppSelector(selectUserData)
  const handleCategoryChange = (categoryId) => {
    setSelectedCategoryId(categoryId)
  }
  // const handleTagsChange = (tagName) => {
  //   setSelectedTagsName(tagName);
  // };
  const handleTagsChange = (tagName) => {
    if (selectedTagsName.includes(tagName)) {
      setSelectedTagsName([])
    } else {
      setSelectedTagsName([tagName])
    }
  }

  // const filteredPostsbyCategory =
  // selectedCategoryId
  //   ? posts.filter((post) => post.category?.id === selectedCategoryId)
  //   : posts;

  // const filteredPostsbyTag =

  // selectedTagsName.length > 0
  //      ? posts.filter((post) =>
  //     post.tags.split(', ').some((tag) => selectedTagsName.includes(tag))
  //   )
  //   : posts;

  const filteredPosts =
    selectedCategoryId || selectedTagsName.length > 0
      ? posts.filter((post) => {
          const categoryMatch =
            !selectedCategoryId || post.category?.id === selectedCategoryId

          const tagsMatch =
            selectedTagsName.length === 0 ||
            post.tags.split(', ').some((tag) => selectedTagsName.includes(tag))

          return categoryMatch && tagsMatch
        })
      : posts
  return (
    <div>
      <Head>
        <title>События которые возвышают</title>
        <meta
          name="description"
          content="События которые возвышают дух собранные в одном месте"
        ></meta>
        <meta charSet="utf-8" />
      </Head>
      <Header />
      <MainLayout>
        <Section1 postsData={posts} />
        <Section2
          postsData={filteredPosts}
          categoriesData={categories}
          tagsData={tags}
          sectionTitle={'Последние посты'}
          handleCategoryChange={handleCategoryChange}
          handleTagsChange={handleTagsChange}
        />
        <Section4 postsData={posts} />
      </MainLayout>
      <Footer />
    </div>
  )
}

export const getServerSideProps = async (ctx) => {
  try {
    await store.dispatch(fetchGetAllPosts())
    await store.dispatch(fetchGetAllCategories())
    await store.dispatch(fetchGetAllTags())
    const posts = store.getState().posts.posts.item
    const categories = store.getState().categories.categories.item
    const tags = store.getState().tags.tags.item
    //console.log('categories',categories)
    return {
      props: {
        posts: posts || [],
        categories: categories || [],
        tags: tags || [],
      },
    }
  } catch (err) {
    console.warn(err)
  }
  return {
    props: {
      posts: [],
      categories: [],
      tags: [],
    },
  }
}

export default Home
