import { MainLayout } from '../../layouts/MainLayout'
import { FullPost } from '../../components/FullPost'
import React from 'react'
import { PostComments } from '../../components/PostComments'
import { Api } from '../../utils/api'
import { GetServerSideProps, NextPage } from 'next'
import { PostItem } from '../../utils/api/types'
import { Related } from '../../components/_childComponent/Ralated'
import { useAppSelector } from '../../redux/hooks'
import { selectUserData, setUserData } from '../../redux/slices/users'
import { store } from '../../redux/store'
import { fetchGetOnePost, fetchGetPopularPosts } from '../../redux/slices/posts'
import PostLayout from '../../layouts/PostLayout'
import { Header } from '../../components/Header'
import { Footer } from '../../components/Footer'

interface FullPostPageProps {
  post: PostItem
  popularPosts: any
}

const FullPostPage: NextPage<FullPostPageProps> = ({ post, popularPosts }) => {
  const userData = useAppSelector(selectUserData)
  return (
    <div>
      <Header />
      <PostLayout title={post.title} description={post.description}>
        <FullPost
          title={post.title}
          subtitle={post.subtitle}
          blocks={post.body}
          views={post.views}
          postId={post.id}
        />
        <Related popularPosts={popularPosts} />
        {userData ? <PostComments postId={post.id} /> : null}
      </PostLayout>
      <Footer />
    </div>
  )
}
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  try {
    const id = ctx.params.id
    await store.dispatch(fetchGetOnePost(id))
    await store.dispatch(fetchGetPopularPosts())
    const post = store.getState().posts.post.item
    const popularPosts = store.getState().posts.popularPosts.item

    return { props: { post, popularPosts } }
  } catch (err) {
    return {
      props: {},
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
}
export default FullPostPage
