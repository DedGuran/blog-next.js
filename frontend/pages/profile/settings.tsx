import React, { useEffect, useState } from 'react'
import {
  Button,
  Divider,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core'
import { MainLayout } from '../../layouts/MainLayout'
import { Api } from '../../utils/api'
import { useRouter } from 'next/router'
import { useDispatch } from 'react-redux'
import { fetchUpdateMe } from '../../redux/slices/users'

export default function Settings() {
  const router = useRouter()
  const dispatch = useDispatch()
  const [isLoading, setIsLoading] = useState(false)

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [status, setStatus] = useState('')
  const onChangeUser = async () => {
    try {
      setIsLoading(true)
      const obj = {
        fullName: name,
        email: email,
        password: password,
        status: status,
      }

      dispatch(fetchUpdateMe(obj))
      await router.push(`/`)
    } catch (err) {
      console.warn(err)
    } finally {
      setIsLoading(false)
    }
  }
  return (
    <MainLayout hideComments>
      <Paper className="p-20 mt-20" elevation={0}>
        <Typography variant="h6">Основные настройки</Typography>
        <Divider className="mt-20 mb-30" />
        <form>
          <TextField
            className="mb-20"
            size="small"
            label="Никнейм"
            value={name}
            onChange={(e) => setName(e.target.value)}
            variant="outlined"
            fullWidth
            required
          />
          <TextField
            className="mb-20"
            size="small"
            label="Эл. почта"
            variant="outlined"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            fullWidth
            required
          />
          <TextField
            className="mb-20"
            size="small"
            label="Пароль"
            variant="outlined"
            fullWidth
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <TextField
            className="mb-20"
            size="small"
            label="Статус"
            variant="outlined"
            value={status}
            onChange={(e) => setStatus(e.target.value)}
            fullWidth
            required
          />
          <Divider className="mt-30 mb-20" />
          <Button
            color="primary"
            variant="contained"
            disabled={isLoading || !email || !name || !password || !status}
            onClick={onChangeUser}
          >
            Сохранить изменения
          </Button>
        </form>
      </Paper>
    </MainLayout>
  )
}
