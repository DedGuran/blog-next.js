import { useEffect, useState } from 'react'
import Link from 'next/link'
import { Paper, Avatar, Typography, Button, Tabs, Tab } from '@material-ui/core'
import {
  SettingsOutlined as SettingsIcon,
  TextsmsOutlined as MessageIcon,
} from '@material-ui/icons'
import Box from '@mui/material/Box'
import { Post } from '../../components/Post'
import { MainLayout } from '../../layouts/MainLayout'
import { fetchGetMe } from '../../redux/slices/users'
import { GetServerSideProps, NextPage } from 'next'
import { store } from '../../redux/store'
import { fetchGetPostsByUserId } from '../../redux/slices/posts'
import { useDispatch, useSelector } from 'react-redux'
import { CommentItem, PostItem } from '../../utils/api/types'
import {
  fetchDeleteComment,
  fetchGetAllComments,
} from '../../redux/slices/comments'
import { IconButton } from '@material-ui/core'
import { Edit, Delete } from '@material-ui/icons'
import * as React from 'react'

import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid'
import FeedbackForm from '../../components/_childComponent/FeedbackForm'

interface ProfileProps {
  //posts: PostItem[],
  id: string
  //comments: CommentItem[]
}

const Profile: NextPage<ProfileProps> = ({ id }) => {
  const dispatch = useDispatch()
  //const data = useSelector(state => state.comments)
  const userData = useSelector((state) => state.user)
  const [activeTab, setActiveTab] = useState(0)
  const [open, setOpen] = useState(false)
  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  function handleDelete(id) {
    if (window.confirm('Удалить комментарий?')) {
      try {
        dispatch(fetchDeleteComment(id))
      } catch (err) {
        console.warn('Error remove comment', err)
      }
    }
    // Implement delete logic here
  }
  const handleTabChange = (event, newValue) => {
    setActiveTab(newValue)
  }

  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 90 },
    {
      field: 'Name',
      headerName: 'Имя пользователя',
      width: 150,
      editable: true,
    },
    {
      field: 'Text',
      headerName: 'Текст комментария',
      width: 150,
      editable: true,
    },
    {
      field: 'LikeCount',
      headerName: 'Количество лайков',
      type: 'number',
      width: 110,
      editable: true,
    },
    {
      field: 'actions',
      headerName: 'Actions',
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <IconButton
              aria-label="delete"
              onClick={() => handleDelete(params.row.id)}
            >
              <Delete />
            </IconButton>
          </>
        )
      },
    },
  ]
  useEffect(() => {
    dispatch(fetchGetMe())
  }, [])

  return (
    <MainLayout contentFullWidth hideComments>
      {userData?.user.item?.role == 'admin' ? (
        <>
          <Paper className="pl-20 pr-20 pt-20 mb-30 mt-10" elevation={0}>
            <div className="d-flex justify-between">
              <div>
                <Avatar
                  style={{ width: 120, height: 120, borderRadius: 10 }}
                  src={
                    userData?.user.item.avatar ? userData?.user.item.avatar : ''
                  }
                />
                <Typography
                  style={{ fontWeight: 'bold' }}
                  className="mt-10"
                  variant="h4"
                >
                  {userData?.user.item.fullName}
                </Typography>
              </div>
              <div>
                <Link href="/profile/settings">
                  <Button
                    style={{
                      height: 42,
                      minWidth: 45,
                      width: 45,
                      marginRight: 10,
                    }}
                    variant="contained"
                  >
                    <SettingsIcon />
                  </Button>
                </Link>
                <Button
                  style={{ height: 42 }}
                  variant="contained"
                  color="primary"
                  onClick={handleClickOpen}
                >
                  <MessageIcon className="mr-10" />
                  Написать
                </Button>
                <FeedbackForm open={open} onClose={handleClose} />
              </div>
            </div>

            <Typography>
              На проекте с{' '}
              {new Date(userData.user.item?.createdAt).toLocaleDateString(
                'ru-RU',
                {
                  month: '2-digit',
                  day: '2-digit',
                  year: 'numeric',
                }
              )}
            </Typography>

            {/* <Tabs className="mt-20" value={activeTab} onChange={handleTabChange} indicatorColor="primary" textColor="primary">
              <Tab label="Статьи" />
              <Tab label="Комментарии" />
              <Tab label="Закладки" />
            </Tabs> */}
          </Paper>
          {/* <div className="d-flex align-start">
            {activeTab === 0 && (<div className="mr-20 flex">
              {posts ? (
                posts?.map(obj => <Post key={obj.id} id={obj.id} title={obj.title} subtitle={obj.subtitle} description={obj.description} category={obj.category} imageUrl={obj.body.find(obj => obj.type === 'image')?.data?.file.url} isEditable={true} views={obj.views} />)
              ) : null}

            </div>)}

            {activeTab === 1 && (<div className="mr-20 flex">
              <Box sx={{ pb: 7 }} >

                
                <DataGrid
                  rows={data.comments ? (
                    data.comments.items?.map(obj => ({ id: obj.id, Name: obj.user.fullName, Text: obj.text, LikeCount: obj.likes.length }))
                  ) : []}
                  columns={columns}
                  initialState={{
                    pagination: {
                      paginationModel: {
                        pageSize: 5,
                      },
                    },
                  }}
                  getRowId={(row) => row.id}
                  pageSizeOptions={[5]}
                  checkboxSelection
                  disableRowSelectionOnClick
                />
              </Box>
            </div>)}

            {activeTab === 2 && (<div className="mr-20 flex">2</div>)}

           
          </div> */}
        </>
      ) : (
        <div>Загрузка данных</div>
      )}
    </MainLayout>
  )
}
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  try {
    const id = ctx.params.id
    //await store.dispatch(fetchGetPostsByUserId(id));
    //await store.dispatch(fetchGetAllComments());
    //await store.dispatch(fetchGetMe());
    //const posts = store.getState().posts.postsByUserId.item;
    //const comments = store.getState().comments.comments.items;

    return {
      props: {
        //posts: posts,
        id: id,
        //comments: comments
      },
    }
  } catch (err) {
    console.warn(err)
  }
  return {
    props: {
      //posts: null,
      id: null,
      //comments: null
    },
  }
}

export default Profile
