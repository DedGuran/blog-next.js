import React, { useEffect } from 'react'
import { NextPage } from 'next'
import AdminLayout from '../../layouts/AdminLayout'
import { BsPersonFill, BsThreeDotsVertical } from 'react-icons/bs'
import { AdminHeader } from '../../components/AdminHeader '
import { store } from '../../redux/store'
import { fetchGetAllUsers } from '../../redux/slices/users'
import { ResponseUser } from '../../utils/api/types'
import { Avatar } from '@material-ui/core'

interface UsersAdminProps {
  users: ResponseUser[]
}
const UsersPage: NextPage<UsersAdminProps> = ({ users }) => {
  return (
    <AdminLayout title={'админ страница'} description={'админ страница'}>
      <AdminHeader title={'Пользователи'} />
      <div className="bg-gray-100 min-h-screen">
        <div className="p-4">
          <div className="w-full m-auto p-4 border rounded-lg bg-white overflow-y-auto">
            <div className="my-3 p-2 grid md:grid-cols-4 sm:grid-cols-3 grid-cols-2 items-center justify-between cursor-pointer">
              <span>Имя</span>
              <span className="sm:text-left text-right">Почта</span>
              <span className="hidden md:grid">Роль</span>
              <span className="hidden md:grid">Опции</span>
            </div>
            <ul>
              {users.map((obj) => (
                <li
                  key={obj.id}
                  className="bg-gray-50 hover:bg-gray-100 rounded-lg my-3 p-2 grid md:grid-cols-4 sm:grid-cols-3 grid-cols-2 items-center justify-between cursor-pointer"
                >
                  {/* <Avatar
                        className=""
                        alt={obj.fullName}
                        src={obj.avatar ? obj.avatar : ""}
                        /> */}
                  <div className="MuiAvatar-root MuiAvatar-circular flex items-center">
                    <img
                      alt={obj.fullName}
                      src={obj.avatar}
                      className="MuiAvatar-img rounded-lg h-10 w-10"
                    />
                    <p className="pl-4">{obj.fullName}</p>
                  </div>
                  <p className="text-gray-600 sm:text-left text-right">
                    {obj.email}
                  </p>
                  <p className="hidden md:flex">{obj.role}</p>
                  <p className="hidden md:flex">
                    <BsThreeDotsVertical />
                  </p>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </AdminLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  try {
    await store.dispatch(fetchGetAllUsers())

    const users = store.getState().user.users.item

    return {
      props: {
        users,
      },
    }
  } catch (err) {
    console.warn(err)
  }
  return {
    props: {
      users: null,
    },
  }
}

export default UsersPage
