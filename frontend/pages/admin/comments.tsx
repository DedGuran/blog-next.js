import React, { useEffect, useState } from 'react'
import { NextPage } from 'next'
import AdminLayout from '../../layouts/AdminLayout'
import { AdminHeader } from '../../components/AdminHeader '
import { store } from '../../redux/store'
import { CommentItem } from '../../utils/api/types'
import { IconButton, Link } from '@material-ui/core'
import { fetchGetAllComments } from '../../redux/slices/comments'
import DeleteIcon from '@mui/icons-material/Clear'
import EditIcon from '@mui/icons-material/Edit'
import { Api } from '../../utils/api'
import router from 'next/router'

interface CommentsAdminProps {
  comments: CommentItem[]
}
const CommentsPage: NextPage<CommentsAdminProps> = ({ comments }) => {
  const [isLoading, setIsLoading] = useState(false)
  const onClickRemove = async (id) => {
    if (window.confirm('Вы действительно хотите удалить комментарий?')) {
      try {
        setIsLoading(true)
        const post = await Api().comment.remove(id)
        await router.push(`/admin/comments`)
      } catch (err) {
        console.warn(err)
      } finally {
        setIsLoading(false)
      }
    }
  }
  return (
    <AdminLayout title={'админ страница'} description={'админ страница'}>
      <AdminHeader title={'Комментарии'} />
      <div className="bg-gray-100 min-h-screen">
        <div className="p-4">
          <div className="w-full m-auto p-4 border rounded-lg bg-white overflow-y-auto">
            <div className="my-3 p-2 grid md:grid-cols-4 sm:grid-cols-3 grid-cols-2 items-center justify-between cursor-pointer">
              <span>Имя</span>
              <span className="sm:text-left text-right">Сообщение</span>
              <span className="sm:text-left text-right">Статья</span>
              <span className="hidden md:grid">Опции</span>
            </div>
            <ul>
              {comments.map((obj) => (
                <li
                  key={obj.id}
                  className="bg-gray-50 hover:bg-gray-100 rounded-lg my-3 p-2 grid md:grid-cols-4 sm:grid-cols-3 grid-cols-2 items-center justify-between cursor-pointer"
                >
                  <div className="MuiAvatar-root MuiAvatar-circular flex items-center">
                    <img
                      alt={obj.user.fullName}
                      src={obj.user.avatar}
                      className="MuiAvatar-img rounded-lg h-10 w-10"
                    />
                    <p className="pl-4">{obj.user.fullName}</p>
                  </div>
                  <p className="text-gray-600 sm:text-left text-right">
                    {obj.text}
                  </p>
                  <p className="text-gray-600 sm:text-left text-right">
                    {obj.post.title}
                  </p>
                  <div className="flex">
                    <p className="text-gray-600 sm:text-left text-right">
                      <IconButton
                        onClick={() => {
                          onClickRemove(obj.id)
                        }}
                        color="secondary"
                      >
                        <DeleteIcon />
                      </IconButton>
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </AdminLayout>
  )
}

export const getServerSideProps = async (ctx) => {
  try {
    await store.dispatch(fetchGetAllComments())

    const comments = store.getState().comments.comments.items

    return {
      props: {
        comments,
      },
    }
  } catch (err) {
    console.warn(err)
  }
  return {
    props: {
      comments: null,
    },
  }
}

export default CommentsPage
