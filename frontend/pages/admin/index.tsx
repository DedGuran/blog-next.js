import React, { useEffect } from 'react'
import { NextPage } from 'next'

import { PostItem } from '../../utils/api/types'
import { useAppSelector } from '../../redux/hooks'
import { selectUserData } from '../../redux/slices/users'
import router from 'next/router'
import AdminLayout from '../../layouts/AdminLayout'
import { AdminHeader } from '../../components/AdminHeader '

import { RecentArticlesAdmin } from '../../components/RecentArticlesAdmin'
import { store } from '../../redux/store'
import { fetchGetAllPosts } from '../../redux/slices/posts'
import { useDispatch, useSelector } from 'react-redux'
import { getSession } from 'next-auth/react'

interface AdminProps {
  posts: PostItem[]
}
const AdminPage: NextPage<AdminProps> = ({ posts }) => {
  const userData = useAppSelector(selectUserData)

  useEffect(() => {
    if (userData && userData?.user.item?.role !== 'admin') {
      // Redirect user to login page if not authenticated
      router.push('/')
    }
  }, [userData])

  return (
    <div>
      <AdminLayout title={'админ страница'} description={'админ страница'}>
        <AdminHeader title={'Статьи'} />

        <RecentArticlesAdmin postsData={posts} />
        {/* <TopCardsAdmin/>
            <BarChartAdmin/> */}
      </AdminLayout>
    </div>
  )
}

export const getServerSideProps = async (ctx) => {
  const session = await getSession({ req: ctx.req })

  if (!session || session.user.role !== 'admin') {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  try {
    await store.dispatch(fetchGetAllPosts())

    const posts = store.getState().posts.posts.item

    return {
      props: {
        posts,
      },
    }
  } catch (err) {
    console.warn(err)
  }
  return {
    props: {
      posts: null,
    },
  }
}

export default AdminPage
