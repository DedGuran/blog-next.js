import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Tabs,
  Tab,
  Button,
} from '@material-ui/core'

import React from 'react'
import { MainLayout } from '../layouts/MainLayout'
import { FollowButton } from '../components/FollowButton'
import { Api } from '../utils/api'
import { ResponseUser } from '../utils/api/types'
import { NextPage } from 'next'

interface RatingPageProps {
  users: ResponseUser[]
}

const Rating: NextPage<RatingPageProps> = ({ users }) => {
  return (
    <MainLayout hideComments>
      <Paper className="pl-20 pt-20 pr-20 mb-20" elevation={0}>
        <Typography
          variant="h5"
          style={{ fontWeight: 'bold', fontSize: 30, marginBottom: 6 }}
        >
          Рейтинг сообществ и блогов
        </Typography>
        <Typography>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, porro
          iure eos nihil dolore similique dignissimos esse quidem dolorum ab
          atque quae numquam veniam? Ut perspiciatis repellendus maxime
          repellat! Voluptates?
        </Typography>
        <Tabs
          className="mt-10"
          value={0}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="Август" />
          <Tab label="За 3 месяца" />
          <Tab label="За все время" />
        </Tabs>
      </Paper>

      <Paper elevation={0}>
        <Table aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Имя пользователя</TableCell>
              <TableCell align="right">Рейтинг</TableCell>
              <TableCell align="right"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((obj) => (
              <TableRow key={obj.id}>
                <TableCell component="th" scope="row">
                  <span className="mr-15">{obj.id}</span>
                  {obj.fullName}
                </TableCell>
                <TableCell align="right">{obj.comment.length * 2}</TableCell>
                <TableCell align="right">
                  <FollowButton />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </MainLayout>
  )
}
export const getServerSideProps = async () => {
  try {
    const users = await Api().user.getAll()

    return {
      props: {
        users,
      },
    }
  } catch (err) {
    console.warn(err)
  }
  return {
    props: {
      users: null,
    },
  }
}

export default Rating
