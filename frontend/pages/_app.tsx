import Head from 'next/head'

import { Header } from '../components/Header'

import { MuiThemeProvider, CssBaseline } from '@material-ui/core'
import { theme } from '../theme'
import { SessionProvider, useSession } from 'next-auth/react'
import '../styles/globals.scss'
//import 'macro-css';
import { wrapper } from '../redux/store'
import { AppProps } from 'next/dist/shared/lib/router/router'
import { fetchGetMe, setUserData } from '../redux/slices/users'
import { Footer } from '../components/Footer'
import { fetchGetAllPosts } from '../redux/slices/posts'
import { Api } from '../utils/api'

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <SessionProvider
        // Provider options are not required but can be useful in situations where
        // you have a short session maxAge time. Shown here with default values.
        session={pageProps.session}
      >
        <Head>
          <title>VedaInfo</title>
          <link rel="icon" href="/favicon.ico" />
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link
            rel="preconnect"
            href="https://fonts.gstatic.com"
            crossOrigin=""
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap"
            rel="stylesheet"
          />
        </Head>
        <MuiThemeProvider theme={theme}>
          <CssBaseline />
          <Component {...pageProps} />
        </MuiThemeProvider>
      </SessionProvider>
    </>
  )
}
App.getInitialProps = wrapper.getInitialAppProps(
  (store) =>
    async ({ ctx, Component }) => {
      try {
        const userData = await Api(ctx).user.getMe()
        store.dispatch(setUserData(userData))
        store.dispatch(fetchGetMe(ctx))
        store.dispatch(fetchGetAllPosts())
      } catch (err) {
        if (ctx.asPath === '/write') {
          ctx.res.writeHead(302, {
            Location: '/',
          })
          ctx.res.end()
        }
        console.log('user not auth')
      }
      return {
        pageProps: Component.getInitialProps
          ? await Component.getInitialProps({ ...ctx, store })
          : {},
      }
    }
)
export default wrapper.withRedux(App)
