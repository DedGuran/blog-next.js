import { MainLayout } from '../../layouts/MainLayout'
import { FullPost } from '../../components/FullPost'
import React, { useEffect, useState } from 'react'
import { PostComments } from '../../components/PostComments'
import { Api } from '../../utils/api'
import { GetServerSideProps, NextPage } from 'next'
import { PostItem } from '../../utils/api/types'
import { Section2 } from '../../components/Section2'
import { store } from '../../redux/store'
import { fetchGetAllPosts } from '../../redux/slices/posts'

interface CategoryProps {
  posts: PostItem[]
  id: string
}

const CategoryPage: NextPage<CategoryProps> = ({ posts, id }) => {
  const [categoryPosts, setCategoryPosts] = useState([])

  const selectPostsByCategoryId = (posts, id) => {
    return posts.filter((post) => post.category.id === +id)
  }

  useEffect(() => {
    setCategoryPosts(selectPostsByCategoryId(posts, id))
  }, [posts])
  return (
    <MainLayout>
      <Section2
        postsData={categoryPosts}
        sectionTitle={categoryPosts[0]?.category.category}
      />
    </MainLayout>
  )
}
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  try {
    const id = ctx.params.id

    await store.dispatch(fetchGetAllPosts())
    const posts = store.getState().posts.posts.item

    return {
      props: {
        posts: posts,
        id: id,
      },
    }
  } catch (err) {
    console.warn(err)
  }
  return {
    props: {
      posts: null,
    },
  }
}
export default CategoryPage
