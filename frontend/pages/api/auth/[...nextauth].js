import NextAuth from 'next-auth'
import GitHubProvider from 'next-auth/providers/github'
import GoogleProvider from 'next-auth/providers/google'
import CredentialsProvider from 'next-auth/providers/credentials'
import { setCookie } from 'nookies'
//import { fetchLogin } from "../../../redux/slices/users";
import { Api } from '../../../utils/api'
import { axiosInstance } from '../../../axios/axios'
import {
  fetchRegistration,
  fetchLogin,
  fetchUserByEmail,
} from '../../../redux/slices/users'

if (!process.env.NEXTAUTH_SECRET) {
  throw new Error('Please provide process.env.NEXTAUTH_SECRET')
}
const serverHost = process.env.NEXT_PUBLIC_FRONTEND_URL
const nextAuthOptions = (req, res) => {
  return {
    secret: process.env.NEXTAUTH_SECRET,
    providers: [
      CredentialsProvider({
        name: 'Credentials',
        id: 'credentials',
        credentials: {
          email: { label: 'Email', type: 'text' },
          password: { label: 'Password', type: 'password' },
        },
        async authorize(credentials) {
          if (!credentials) {
            throw new Error('No credentials.')
          }
          const { email, password } = credentials

          try {
            const objRegistration = {
              fullName: email,
              email: email,
              password: password,
              avatar: '',
              role: email == process.env.ADMIN_EMAIL_ADDRESS ? 'admin' : 'user',
            }

            //const registerUser = store.dispatch(fetchRegistration(objRegistration))
            const registerUser = await Api().user.register(objRegistration)
            //const registerUser = await axiosInstance().post(`/auth/register`, objRegistration)

            if (registerUser) {
              setCookie({ res }, 'token', registerUser.token, {
                path: '/',
                maxAge: 30 * 24 * 60 * 60,
                httpOnly: false,
                secure: false,
              })
              return registerUser
            } else {
              return null
            }
          } catch (err) {
            try {
              const objLogin = {
                email: email,
                password: password,
              }
              //const loginUser = await dispatch(fetchLogin(objLogin))
              const loginUser = await Api().user.login(objLogin)
              //const loginUser = await axiosInstance().post(`/auth/login`, objRegistration)

              if (loginUser) {
                setCookie({ res }, 'token', loginUser.token, {
                  path: '/',
                  maxAge: 30 * 24 * 60 * 60,
                  httpOnly: false,
                  secure: false,
                })
                return loginUser
              } else {
                return null
              }
            } catch (error) {
              console.log(error)
            }
          }
        },
      }),
      GitHubProvider({
        clientId: process.env.GITHUB_ID,
        clientSecret: process.env.GITHUB_SECRET,
      }),
      GoogleProvider({
        clientId: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      }),
    ],

    callbacks: {
      signIn: async ({ user, account, profile, email, credentials }) => {
        try {
          const checkUser = await Api().user.getUserByEmail(user.email)
          //const checkUser = await store.dispatch(fetchUserByEmail(user.email))
          //const checkUser = await axiosInstance().get(`/users/me/${email}`)
          if (!checkUser) {
            const regObj = {
              fullName:
                account.provider === 'google' || account.provider === 'github'
                  ? user.name
                  : user.email,
              email: user.email,
              password:
                account.provider === 'google' || account.provider === 'github'
                  ? user.email
                  : user.fullName,
              avatar: user.image,
              role:
                user.email == process.env.ADMIN_EMAIL_ADDRESS
                  ? 'admin'
                  : 'user',
            }
            const registerUser = await Api().user.register(regObj)
            //const registerUser = await axiosInstance().post(`/auth/register`, regObj)

            if (registerUser) {
              // Set a cookie on the client-side

              setCookie({ res }, 'token', registerUser.token, {
                path: '/',
                maxAge: 30 * 24 * 60 * 60,
                httpOnly: false,
                secure: false,
              })
              return registerUser
            } else {
              return null
            }
          } else {
            // const loginUser = await dispatch(fetchLogin({
            //   "email": email,
            //   "password": password
            // }))
            const logObj = {
              email: checkUser.email,
              password: checkUser.password,
            }
            const loginUser = await Api().user.login(logObj)
            //const loginUser = await axiosInstance().post(`/auth/login`, logObj)
            if (loginUser) {
              setCookie({ res }, 'token', loginUser.token, {
                path: '/',
                maxAge: 30 * 24 * 60 * 60,
                httpOnly: false,
                secure: false,
              })
              return loginUser
            } else {
              return null
            }
          }
        } catch (err) {
          console.log('не получилось войти с next-auth', err)
        }
        return true
      },
      jwt: async ({ token, user }) => {
        if (user) {
          token.id = user.id
          token.role = user.role
        }
        return token
      },
      session: async ({ session, token }) => {
        session.user.id = token.id
        session.user.role = token.role
        return session
      },
      redirect: async (url) => {
        return `${serverHost}`
      },
    },
  }
}

export default (req, res) => {
  return NextAuth(req, res, nextAuthOptions(req, res))
}
