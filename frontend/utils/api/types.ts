import { OutputData } from '@editorjs/editorjs'
export type LoginDto = {
  email: string
  password: string
}
export type CreateUserDto = {
  fullName: string
  avatar?: string
  role: string
} & LoginDto

export type ResponseUser = {
  user: any
  createdAt: string
  email: string
  fullName: string
  id: number
  comment?: any
  //commentsCount?: number,
  token: string
  updatedAt: string
  avatar?: string
  role: string
  status: string
}
export type ResponseCategory = {
  id: number
  category: string
  createdAt: string
  updatedAt: string
}
export type ResponseTags = {
  string
}
export type PostItem = {
  title: string
  body: OutputData['blocks']
  tags: null | string
  id: number
  description: string
  user: ResponseUser
  category: ResponseCategory
  views: number
  createdAt: string
  updatedAt: string
  keywords: string
  subtitle: string
}

export type CommentItem = {
  id: number
  text: string
  post: PostItem
  tags: null | string
  user: ResponseUser
  views: number
  createdAt: string
  updatedAt: string
  active: boolean
  likes: CommentLikeItem[]
}
export type CommentLikeItem = {
  userId: number
  id: number
}
export type FavoritesItem = {
  userId: number
  id: number
}
export type RemoveImageDto = {
  status: string
  urlName: string
}
