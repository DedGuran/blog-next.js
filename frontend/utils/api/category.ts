import { AxiosInstance } from 'axios'
import { ResponseCategory } from './types'

type CreateCategoryDto = {
  category: string
}

export const CategoryApi = (instance: AxiosInstance) => ({
  async getAll() {
    const { data } = await instance.get<ResponseCategory[]>('/category')
    return data
  },

  async create(dto: CreateCategoryDto) {
    const { data } = await instance.post<
      CreateCategoryDto,
      { data: ResponseCategory }
    >('/category', dto)

    return data
  },
  async remove(id: number) {
    const { data } = await instance.delete<any, { data: ResponseCategory }>(
      `/category/${id}`
    )
    return data
  },
})
