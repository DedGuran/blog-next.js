import { OutputData } from '@editorjs/editorjs'
import { AxiosInstance } from 'axios'
import { PostItem, RemoveImageDto, ResponseUser } from './types'

type CreatePostDto = {
  title: string
  body: OutputData['blocks'] | null
}
type UploadImgDto = {
  formData: any
}
type SearchPostDto = {
  title?: string
  subtitle?: string
  body?: string
  views?: 'DESC' | 'ASC'
  limit?: number
  take?: number
  tag?: string
}

export const PostApi = (instance: AxiosInstance) => ({
  async getAll() {
    const { data } = await instance.get<PostItem[]>('/posts')
    return data
  },
  async getPopularPosts() {
    const { data } = await instance.get<PostItem[]>('/posts/popular')
    return data
  },
  async search(query: SearchPostDto) {
    const { data } = await instance.get<{ items: PostItem[]; total: number }>(
      '/posts/search',
      { params: query }
    )
    return data
  },
  async getOne(id: number) {
    const { data } = await instance.get<PostItem, { data: ResponseUser }>(
      `/posts/${id}`
    )
    return data
  },
  async create(dto: CreatePostDto) {
    const { data } = await instance.post<CreatePostDto, { data: PostItem }>(
      '/posts',
      dto
    )

    return data
  },
  async update(id: number, dto: CreatePostDto) {
    const { data } = await instance.patch<CreatePostDto, { data: PostItem }>(
      `/posts/${id}`,
      dto
    )

    return data
  },
  async remove(id: number) {
    const { data } = await instance.delete<PostItem, { data: ResponseUser }>(
      `/posts/${id}`
    )
    return data
  },
  async imageUpload(uploadDto: any) {
    const formData = new FormData()
    formData.append('image', uploadDto)
    const { data } = await instance.post<any, { data: any }>(
      `/posts/uploadImage`,
      formData
    )
    return data
  },
  async removeImage(imageName: string) {
    const { data } = await instance.delete<any, { data: RemoveImageDto }>(
      `/posts/uploadImage/${imageName}`
    )
    return data
  },
  async getAllByUserId(id: string) {
    const { data } = await instance.get<PostItem[]>(`/posts/user/${id}`)
    return data
  },
})
