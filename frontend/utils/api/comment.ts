import { OutputData } from '@editorjs/editorjs'
import { AxiosInstance } from 'axios'
import { CommentItem, CommentLikeItem, PostItem, ResponseUser } from './types'

type CreateCommentDto = {
  postId: number
  text: string
  active: boolean
}
type CreateCommentLikeDto = {
  userId: number
  commentId: number
}
export const CommentApi = (instance: AxiosInstance) => ({
  async getAll(postId: number) {
    const { data } = await instance.get<CommentItem[]>('/comments', {
      params: { postId },
    })
    return data
  },
  async create(dto: CreateCommentDto) {
    const { data } = await instance.post<
      CreateCommentDto,
      { data: CommentItem }
    >(`/comments`, dto)
    return data
  },
  async remove(id: number) {
    return instance.delete(`/comments/` + id)
  },
  async createLike(dto: CreateCommentLikeDto) {
    const { data } = await instance.post<
      CreateCommentLikeDto,
      { data: CommentLikeItem }
    >(`/commentlike`, dto)
    return data
  },
  async deleteLike(id: number) {
    return instance.delete(`/commentlike/` + id)
  },
})
