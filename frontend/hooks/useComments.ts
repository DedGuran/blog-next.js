import React, { useEffect, useState } from 'react'
import { comments } from '../data'
import { Api } from '../utils/api'
import { CommentItem } from '../utils/api/types'

type UseCommentsProps = {
  setComments: React.Dispatch<React.SetStateAction<CommentItem[]>>
  comments: CommentItem[]
}

export const useComments = (postId?: number): UseCommentsProps => {
  const [comments, setComments] = useState<CommentItem[]>([])
  useEffect(() => {
    ;(async () => {
      try {
        const arr = await Api().comment.getAll(postId)

        setComments(arr.filter((obj) => obj.active == true))
      } catch (err) {
        console.warn('Fetch comments', err)
      }
    })()
  }, [])
  return { comments, setComments }
}
