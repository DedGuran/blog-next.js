import React, { useEffect, useState } from 'react'
import { comments } from '../data'
import { Api } from '../utils/api'
import { ResponseCategory } from '../utils/api/types'

type UseCategoryProps = {
  setCategory: React.Dispatch<React.SetStateAction<ResponseCategory[]>>
  category: ResponseCategory[]
}

export const useCategory = (): UseCategoryProps => {
  const [category, setCategory] = useState<ResponseCategory[]>([])
  useEffect(() => {
    ;(async () => {
      try {
        const arr = await Api().category.getAll()
        setCategory(arr)
      } catch (err) {
        console.warn('Fetch category', err)
      }
    })()
  }, [])
  return { category, setCategory }
}
