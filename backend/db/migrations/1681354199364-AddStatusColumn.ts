import { MigrationInterface, QueryRunner } from "typeorm";

export class AddStatusColumn1681354199364 implements MigrationInterface {
    name = 'AddStatusColumn1681354199364'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" ADD "status" varchar(255) NOT NULL DEFAULT 'default_value'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "status"`);
    }

}
