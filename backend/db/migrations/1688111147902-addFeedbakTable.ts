import { MigrationInterface, QueryRunner } from "typeorm";

export class AddFeedbakTable1688110685687 implements MigrationInterface {
    name = 'AddFeedbakTable1688110685687'

    public async up(queryRunner: QueryRunner): Promise<void> {
        const hasStatusColumn = await queryRunner.hasColumn("users", "status");
        
        if (!hasStatusColumn) {
            await queryRunner.query(`ALTER TABLE "users" ADD "status" character varying NOT NULL DEFAULT ''`);
        }

        await queryRunner.query(`CREATE TABLE "feedback" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "email" character varying NOT NULL, "text" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_8389f9e087a57689cd5be8b2b13" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "feedback"`);

        const hasStatusColumn = await queryRunner.hasColumn("users", "status");

        if (hasStatusColumn) {
            await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "status"`);
        }
    }
}
