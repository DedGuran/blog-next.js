import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class Favoriteposts1694072944332 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
          new Table({
            name: 'favoriteposts',
            columns: [
              { name: 'id', type: 'serial', isPrimary: true },
              { name: 'userId', type: 'int' },
              { name: 'postId', type: 'int' },
              { name: 'createdAt', type: 'timestamp', default: 'CURRENT_TIMESTAMP' },
            ],
            foreignKeys: [
              {
                columnNames: ['userId'],
                referencedTableName: 'users',
                referencedColumnNames: ['id'],
                onDelete: 'CASCADE',
              },
              {
                columnNames: ['postId'],
                referencedTableName: 'posts',
                referencedColumnNames: ['id'],
                onDelete: 'CASCADE',
              },
            ],
          }),
        );
      }
    
      public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('favoriteposts');
      }


}
