import { DataSource, DataSourceOptions } from "typeorm"

export const dataSourceOptions:DataSourceOptions={
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'guran',
    password: 'radhakrishna',
    database: 'nest',
    entities: ['dist/**/*.entity.js'],
    migrations: ['dist/db/migrations/*.js'],
    migrationsRun: true,
}

const dataSource =new DataSource(dataSourceOptions)

export default dataSource