import { Injectable, NotFoundException,Body } from '@nestjs/common';
import { CreateFeedbackDto } from './dto/create-feedback.dto';
import { UpdateFeedbackDto } from './dto/update-feedback.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { FeedbackEntity } from './entities/feedback.entity';
import { Repository } from 'typeorm';

@Injectable()
export class FeedbackService {
  constructor(
    @InjectRepository(FeedbackEntity)
    private repository: Repository<FeedbackEntity>
  ) { }
  create(@Body() createFeedbackDto: CreateFeedbackDto) {
    return this.repository.save(createFeedbackDto)
  }

  findAll() {
    return this.repository.find({
      order: {
        createdAt: "DESC"
      }
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} feedback`;
  }

  update(id: number, updateFeedbackDto: UpdateFeedbackDto) {
    return `This action updates a #${id} feedback`;
  }

  async remove(id: number) {
    const find = await this.repository.findOne({ where: { id: +id } })
   


    if (!find) {
      throw new NotFoundException("Сообщение не найдено!")
    }
    
   
    return this.repository.delete(id);
  }
}
