import { Injectable,NotFoundException,ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CommentEntity } from 'src/comment/entities/comment.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { SearchUserDto } from './dto/search-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private repository: Repository<UserEntity>
  ) { }
  create(dto: CreateUserDto) {
    return this.repository.save(dto);
  }

  async findAll() {
    const arr = await this.repository
      .createQueryBuilder('u')
      .leftJoinAndMapMany('u.comment', CommentEntity, 'comment', 'comment.userId=u.id')
      //.loadRelationCountAndMap('u.commentsCount', 'u.comment', 'comment')
      .getMany();

    return arr.map(obj => {
      delete obj.comments;
      return obj;
    })
  }

  findById(id: number) {
    return this.repository.findOne({ where: { id: +id } });
  }
  findByCond(cond: LoginUserDto): Promise<UserEntity> {
    const { email } = cond;
    return this.repository.findOne({ where: { email } });
  }
  async getUserByEmail(email: string) {
    return this.repository.findOne({ where: { email: email } });
  }
  async update(id: number, dto: UpdateUserDto) {
    //return this.repository.update(id, dto);
    const find = await this.repository.findOne({ where: { id: +id } })

    if (!find) {
      throw new NotFoundException("Пользователь не найден!")
    }
    
    return this.repository.update(id, {
      fullName: dto.fullName,
      email: dto.email,
      password: dto.password,
      status: dto.status,  
    });
  }

  async search(dto: SearchUserDto) {
    const qb = this.repository.createQueryBuilder('u')

    qb.limit(dto.limit || 0)
    qb.take(dto.take || 10)


    if (dto.fullName) {
      qb.andWhere(`u.fullName ILIKE :fullName`)
    }

    if (dto.email) {
      qb.andWhere(`u.email ILIKE :email`)
    }

    qb.setParameters({
      email: `%${dto.email}%`,
      fullName: `%${dto.fullName}%`,

    })
    const [items, total] = await qb.getManyAndCount();

    return { items, total }
  }
  async remove(id: number) {
    const find = await this.repository.findOne({ where: { id: id } })
    
    if (!find) {
      throw new NotFoundException("Пользователь не найден!")
    }
    return this.repository.delete(id);
  }
}
