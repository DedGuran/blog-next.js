import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { IsEmail } from 'class-validator';
import { CommentEntity } from 'src/comment/entities/comment.entity';

@Entity("users")
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullName: string;

  @Column({
    unique: true,
  })
  @IsEmail()
  email: string;

  @OneToMany(() => CommentEntity, (comment) => {
    comment.user, {
      eager: false,
      nullable: true
    }
  })
  comments: CommentEntity[];

  @Column({ nullable: true })
  password?: string;

  @Column()
  avatar?: string;
  
  @Column()
  role?: string;

  @Column()
  status: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}