import { IsEmail, Length } from 'class-validator';
import { UniqueOnDatabase } from 'src/auth/validations/UniqueValidation';
import { UserEntity } from '../entities/user.entity';


export class UpdateUserDto {
    @Length(3)
    fullName: string;

    @IsEmail(undefined, { message: 'Неверная почта' })
    email: string;

    @Length(6, 32, { message: 'Пароль должен быть минимум 6 символов' })
    password: string;


    avatar?: string;
    role?: string;
    status?: string;
}
