import { PostEntity } from 'src/post/entities/post.entity';
import { Entity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, Column } from 'typeorm';
@Entity("category")
export class CategoryEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    category: string;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date
}
