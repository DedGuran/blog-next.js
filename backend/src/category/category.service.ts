import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreatePostDto } from 'src/post/dto/create-post.dto';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CategoryEntity } from './entities/category.entity';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(CategoryEntity)
    private repository: Repository<CategoryEntity>
  ) { }
  create(dto: any) {

    return this.repository.save(dto);
  }

  findAll() {
    return this.repository.find({
      order: {
        createdAt: "DESC"
      }
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} category`;
  }

  update(id: number, updateCategoryDto: UpdateCategoryDto) {
    return `This action updates a #${id} category`;
  }

  async remove(id: number, userId: number) {
    const find = await this.repository.findOne({ where: { id: +id } })

    if (!find) {
      throw new NotFoundException("Категория не найдена!")
    }

    return this.repository.delete(id);
  }
}
