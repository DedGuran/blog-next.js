import { IsEmail, Length } from 'class-validator';

export class CreateCategoryDto {
    @Length(3)
    category: string;
}
