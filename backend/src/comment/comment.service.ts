import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { CommentEntity } from './entities/comment.entity';
import { CommentlikeEntity } from 'src/commentlike/entities/commentlike.entity';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(CommentEntity)
    private repository: Repository<CommentEntity>
  ) {}
  private async removeCommentLikes(commentId: number) {
    await this.repository
      .createQueryBuilder()
      .delete()
      .from(CommentlikeEntity)
      .where('commentId = :id', { id: commentId })
      .execute();
  }
  async create(dto: CreateCommentDto, userId: number) {
    const comment = await this.repository.save({
      text: dto.text,
      active: dto.active,
      post: { id: dto.postId },
      user: { id: userId },
    });
    const qb = this.repository
      .createQueryBuilder('c')
      .where({ id: comment.id });
    const arr = await qb
      .leftJoinAndSelect('c.user', 'user')
      .leftJoinAndSelect('c.likes', 'like')
      .getMany();

    return arr.map((obj) => {
      return {
        ...obj,
        text: obj.text,
        active: obj.active,

        user: {
          id: obj.user.id,
          fullName: obj.user.fullName,
          avatar: obj.user.avatar,
          createdAt: obj.user.createdAt,
          updatedAt: obj.user.updatedAt,
        },
      };
    });
  }

  async findAll(postId: number) {
    const qb = this.repository.createQueryBuilder('c');
    if (postId) {
      qb.where({ post: { id: postId } });
    }
    const arr = await qb
      .leftJoinAndSelect('c.post', 'post')
      .leftJoinAndSelect('c.user', 'user')
      .leftJoinAndSelect('c.likes', 'like')
      .groupBy('c.id, post.id, user.id, like.id')
      .getMany();

    return arr.map((obj) => {
      return {
        ...obj,
        text: obj.text,
        active: obj.active,
        post: { id: obj.post.id, title: obj.post.title },
        user: {
          id: obj.user.id,
          fullName: obj.user.fullName,
          avatar: obj.user.avatar,
          createdAt: obj.user.createdAt,
          updatedAt: obj.user.updatedAt,
        },
      };
    });
  }

  findOne(id: number) {
    return this.repository.findOne({ where: { id: +id } });
  }
  async findOneByPostId(postId: number) {
    const qb = this.repository
      .createQueryBuilder('c')
      .where({ post: { id: postId } });
    const arr = await qb
      .leftJoinAndSelect('c.user', 'user')
      .leftJoinAndSelect('c.likes', 'like')
      .getMany();
    //return await this.repository.query('SELECT * FROM comments')
    return arr.map((obj) => {
      return {
        ...obj,
        text: obj.text,
        active: obj.active,
        user: {
          id: obj.user.id,
          fullName: obj.user.fullName,
          createdAt: obj.user.createdAt,
          updatedAt: obj.user.updatedAt,
        },
      };
    });
    return this.repository.find({ where: { post: { id: postId } } });
  }

  update(id: number, dto: UpdateCommentDto) {
    return this.repository.update(id, dto);
  }

  async remove(id: number) {
    await this.removeCommentLikes(id);
    const deletedComment = this.repository.delete(id);
    return {
      id: id,
    };
  }
}
