import { ForbiddenException, Injectable } from '@nestjs/common';
import { UserEntity } from 'src/user/entities/user.entity';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AuthService {

  constructor(
  
    private userService: UserService,
    private jwtService: JwtService,
  ) { }

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.userService.findByCond({
      email,
      password,
    });
    if (user && user.password === password) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
  generateJwtToken(data: { id: number; email: string }) {
    const payload = { email: data.email, sub: data.id };
    return this.jwtService.sign(payload);
  }
  async login(user: UserEntity) {
    const { password, ...userData } = user;
    return {
      ...userData,
      token: this.generateJwtToken(userData),
    };
  }

  async register(dto: CreateUserDto) {
    try {

      const { password, ...user } = await this.userService.create({
        email: dto.email,
        fullName: dto.password,
        password: dto.password,
        avatar:dto.avatar,
        role:dto.role
      });

      return {
        ...user,
        token: this.generateJwtToken(user),
      };
    } catch (err) {

      throw new ForbiddenException('Ошибка при регистрации');
    }
  }
  async findAll() {
    return await this.userService.findAll();
  }
}
