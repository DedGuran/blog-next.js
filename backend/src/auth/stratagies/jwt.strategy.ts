import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';

import { UserService } from 'src/user/user.service';
import { UnsubscriptionError } from 'rxjs';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: "radhakrishna",
    });
  }

  async validate(payload: { sub: number; email: string }) {
    const data = { id: payload.sub, email: payload.email }
    const user = await this.userService.findByCond(data);//Проверяем что такой пользователь есть
    if (!user) {
      throw new UnauthorizedException("Нет доступа к этой странице!")
    }
    return {
      id: user.id,
      email: user.email,

    };
  }
}