import { HttpException, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from 'src/app.controller';
import { AppService } from 'src/app.service';

import { UserModule } from './user/user.module';
import { PostModule } from './post/post.module';

import { CommentModule } from './comment/comment.module';

import { AuthModule } from './auth/auth.module';
import { CategoryModule } from './category/category.module';

import { MulterModule } from '@nestjs/platform-express';
import { CommentlikeModule } from './commentlike/commentlike.module';

import { dataSourceOptions } from 'db/data-source';
import { FeedbackModule } from './feedback/feedback.module';
import { FavoritepostsModule } from './favoriteposts/favoriteposts.module';

@Module({
  imports: [
    MulterModule.register({
      dest: './upload',
    }),

    TypeOrmModule.forRoot(dataSourceOptions),
    UserModule,
    PostModule,
    CommentModule,
    AuthModule,
    CategoryModule,
    CommentlikeModule,
    FeedbackModule,
    FavoritepostsModule,
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
