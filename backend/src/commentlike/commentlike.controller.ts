import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { CommentlikeService } from './commentlike.service';
import { UpdateCommentlikeDto } from './dto/update-commentlike.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CreateCommentlikeDto } from './dto/create-commentlike.dto';

@Controller('commentlike')
export class CommentlikeController {
  constructor(private readonly commentlikeService: CommentlikeService) {}
  
  
  @Post()
  async createLike(@Body() createCommentLikeDto: CreateCommentlikeDto) {
    
    return await this.commentlikeService.createLike(createCommentLikeDto);
  }

  @Get()
  findAll() {
    return this.commentlikeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.commentlikeService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCommentlikeDto: UpdateCommentlikeDto) {
    return this.commentlikeService.update(+id, updateCommentlikeDto);
  }

  @Delete(':id')
  async deleteLike(@Param('id') likeId: number): Promise<void> {
   
    return await this.commentlikeService.deleteLike(likeId);
  }
}
