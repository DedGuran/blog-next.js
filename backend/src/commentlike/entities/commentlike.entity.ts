
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { CommentEntity } from '../../comment/entities/comment.entity';

@Entity("commentlike")
export class CommentlikeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  userId: number;

  @ManyToOne(() => CommentEntity, { eager: true })

  comment: CommentEntity;
}