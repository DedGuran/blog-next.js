import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCommentlikeDto } from './dto/create-commentlike.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateCommentlikeDto } from './dto/update-commentlike.dto';
import { CommentlikeEntity } from './entities/commentlike.entity';


@Injectable()
export class CommentlikeService {

  constructor(
    @InjectRepository(CommentlikeEntity)
    private repository: Repository<CommentlikeEntity>
  ) { }
  async createLike(dto): Promise<CommentlikeEntity> {
    const userId = dto.userId
    const commentId = dto.commentId
    const existingLike = await this.repository.findOne({
      where: { userId, comment: { id: commentId } },
    });
    if (existingLike) {
      throw new Error('User has already liked the comment');
    }
    const like = new CommentlikeEntity();
    like.userId = userId;
    like.comment = { id: commentId } as any;
    return await this.repository.save(like);
  }


  findAll() {
    return this.repository.find({});
  }

  findOne(id: number) {
    return `This action returns a #${id} commentlike`;
  }

  update(id: number, updateCommentlikeDto: UpdateCommentlikeDto) {
    return `This action updates a #${id} commentlike`;
  }

  async deleteLike(likeId: number): Promise<any> {

    const like = await this.repository.findOne({ where: { id: +likeId } });
    if (!like) {
      throw new NotFoundException(`Like with ID ${likeId} not found`);
    }
    const deletedLike = await this.repository.remove(like);
    
    return {
      id: +likeId,
      userId: deletedLike.userId,
      comment: deletedLike.comment,
    };

  }
}
