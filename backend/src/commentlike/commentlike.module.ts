import { Module } from '@nestjs/common';
import { CommentlikeService } from './commentlike.service';
import { CommentlikeController } from './commentlike.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentlikeEntity } from './entities/commentlike.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CommentlikeEntity])],
  controllers: [CommentlikeController],
  providers: [CommentlikeService]
})
export class CommentlikeModule {}
