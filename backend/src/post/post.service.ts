import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostEntity } from './entities/post.entity';
import { SearchPostDto } from './dto/search-post.dto';
import * as fs from 'fs'

@Injectable()
export class PostService {

  constructor(
    @InjectRepository(PostEntity)
    private repository: Repository<PostEntity>
  ) { }
  create(dto: CreatePostDto, userId: number) {

    const firstParagraph = dto.body.find(obj => obj.type === 'paragraph')?.data?.text;

    return this.repository.save({
      title: dto.title,
      subtitle: dto.subtitle,
      keywords: dto.keywords,
      body: dto.body,
      tags: dto.tags,
      category: dto.category,
      user: { id: userId },
      description: firstParagraph || '',

    })
  }
  createImage(dto: any) {

    //console.log('dto', dto)
    return {
      success: 1,
      file: {
        "url": `http://localhost:4000/posts/${dto.path}`,
      }
    }
  }
  getImage(res: any, image: string) {
    const response = res.sendFile(image, { root: './upload' });
    //console.log('dto', dto)
    return {
      success: 1,
      data: response,
    }
  }
  async removeImage(dto: string) {
    await fs.unlink(`./upload/${dto}`, (err) => {
      if (err) {
        console.error(err);
        return err;
      }

    });
    return {
      success: 1,
      urlName: `${dto}`
    }
  }
  async popular() {
    const qb = this.repository.createQueryBuilder()
    qb.select(['post', 'category.id', 'category.category', 'user.fullName', 'user.avatar', 'user.role', 'user.status'])
      .from(PostEntity, 'post')
      .leftJoin('post.category', 'category')
      .leftJoin('post.user', 'user')
      .orderBy("post.views", "DESC")
      .limit(10);
    const [items, total] = await qb.getManyAndCount()

    return {
      items, total
    }
  }

  findAll() {
    return this.repository.find({
      order: {
        createdAt: "DESC"
      }
    });
  }

  async search(dto: SearchPostDto) {
    const qb = this.repository.createQueryBuilder('p')
    qb.leftJoinAndSelect('p.user', 'user')
    qb.limit(dto.limit || 0)
    qb.take(dto.take || 10)

    if (dto.views) {
      qb.orderBy("views", dto.views)
    }


    if (dto.body) {
      qb.andWhere(`p.body ILIKE :body`)
    }

    if (dto.title) {
      qb.andWhere(`p.title ILIKE :title`)
    }

    if (dto.tag) {
      qb.andWhere(`p.tags ILIKE :tag`)
    }
    if (dto.tag) {
      qb.andWhere(`p.subtitle ILIKE :subtitle`)
    }

    qb.setParameters({
      title: `%${dto.title}%`,
      subtitle: `%${dto.subtitle}%`,
      body: `%${dto.body}%`,
      tag: `%${dto.tag}%`,
      views: dto.views || "DESC"
    })
    const [items, total] = await qb.getManyAndCount();

    return { items, total }
  }

  async findOne(id: number) {


    await this.repository.createQueryBuilder("posts").whereInIds(id).update().set({ views: () => 'views+1' }).execute()
    return this.repository.findOne({ where: { id: +id } })
  }

  async update(id: number, dto: UpdatePostDto, userId: number) {
    const find = await this.repository.findOne({ where: { id: +id } })

    if (!find) {
      throw new NotFoundException("Статья не найдена!")
    }
    const firstParagraph = dto.body.find(obj => obj.type === 'paragraph')?.data?.text;
    return this.repository.update(id, {
      title: dto.title,
      subtitle: dto.subtitle,
      keywords: dto.keywords,
      body: dto.body,
      tags: dto.tags,
      user: { id: userId },
      description: firstParagraph || '',

    });
  }

  async remove(id: number, userId: number) {
    const find = await this.repository.findOne({ where: { id: +id } })
    const images = await find.body.filter(obj => obj.type === 'image')


    if (!find) {
      throw new NotFoundException("Статья не найдена!")
    }
    if (find.user.id !== userId) {
      throw new ForbiddenException('Нет доступа к этой статье')
    }
    if (images.length > 0) {
      images.forEach(el => {

        fs.unlink(`./upload/${el.data.file.url.split('/').slice(-1).join()}`, (err) => {
          if (err) {
            console.error(err);
            return err;
          }

        });
      })
    }
    return this.repository.delete(id);
  }


  async getPostsByUserId(id: string) {
    return this.repository
      .createQueryBuilder('posts')
      .where('posts.user = :id', { id })
      .getMany();
  }
  async getPostsByCategoryId(id: string) {
    return this.repository
      .createQueryBuilder('posts')
      .where('posts.category = :id', { id })
      .getMany();
  }
 
  async getTags() {
    try {
      const posts = await this.repository.find({ order: { createdAt: "DESC" } });
      const tags = posts.map(obj => obj.tags).flat()
      return tags
    } catch (err) { console.log(err) }

  }
}
