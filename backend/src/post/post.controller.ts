import { Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, UploadedFile, UseInterceptors, Res } from '@nestjs/common';
import { PostService } from './post.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { SearchPostDto } from './dto/search-post.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { User } from 'src/decorators/user.decorator';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';

import { AnyFilesInterceptor, FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import path = require('path');



@Controller('posts')
export class PostController {
  constructor(private readonly postService: PostService) { }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@User() userId: number, @Body() createPostDto: CreatePostDto) {

    return this.postService.create(createPostDto, userId);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/uploadImage')
  @UseInterceptors(FileInterceptor('image', {
    storage: diskStorage({
      destination: './upload',
      filename: (req: any, file: any, cd: (arg0: any, arg1: string) => void) => {

        const filename: string = path.parse(file.originalname).name.replace(/\s/g, '') + uuidv4()
        const extansion: string = path.parse(file.originalname).ext

        cd(null, `${filename}${extansion}`)
      }
    })
  }))
  createImage(@UploadedFile() file: any) {
    try {

      return this.postService.createImage(file);
    } catch (err) {
      console.log('err', err)
    }

  }
  @Get('/upload/:imagename')
  getImage(@Param('imagename') image, @Res() res) {
    try {

      return this.postService.getImage(res, image);
    } catch (err) {
      console.log('err', err)
    }

  }
  @UseGuards(JwtAuthGuard)
  @Delete('/uploadImage/:imageName')
  async removeImage(@Param('imageName') imageName: string) {
    try {
      return this.postService.removeImage(imageName);
    } catch (err) {
      console.log('err', err)
    }

  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@User() userId: number, @Param('id') id: string, @Body() updatePostDto: UpdatePostDto) {
    return this.postService.update(+id, updatePostDto, userId);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@User() userId: number, @Param('id') id: string) {
    return this.postService.remove(+id, userId);
  }

  @Get()
  findAll() {
    return this.postService.findAll();
  }

  @Get("/popular")
  getPopularPosts() {
    return this.postService.popular();
  }
  @Get("/tags")
  getLastTags() {
    return this.postService.getTags()
  }
 
  @Get("/search")
  searchPosts(@Query() dto: SearchPostDto) {

    return this.postService.search(dto);
  }


  @Get('/user/:id')
  async getPostsByUserId(@Param('id') id: string) {
    return this.postService.getPostsByUserId(id);
  }
  @Get('/category/:id')
  async getPostsByCategoryId(@Param('id') id: string) {
    return this.postService.getPostsByCategoryId(id);
  }
  @Get(':id')
  findOne(@Param('id') id: string) {


    return this.postService.findOne(+id);
  }


}

