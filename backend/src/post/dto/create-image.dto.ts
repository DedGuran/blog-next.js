import { Length, IsString, IsArray, IsOptional, IsNumber } from 'class-validator';


export class CreateImageDto {
    @IsNumber()
    lastModified: number;

    lastModifiedDate: any;

    @IsString()
    name: string;

    @IsNumber()
    size: number;

    @IsString()
    type: string;

    @IsString()
    webkitRelativePath: string;
}
