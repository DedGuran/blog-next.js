

export class SearchPostDto {
    title?: string;
    subtitle?: string;
    body?: string;
    views?: "DESC" | "ASC";
    limit?: number;
    take?: number;
    tag?: string;

}
