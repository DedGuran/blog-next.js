import { Length, IsString, IsArray, IsOptional, IsNumber } from 'class-validator';

export interface OutputBlockData {
    id?: string,
    type: 'paragraph' | string,
    data: any

}

export class CreatePostDto {
    @IsString()
    title: string;

    @IsString()
    subtitle: string;

    @IsString()
    keywords: string;

    @IsArray()
    body: OutputBlockData[];

    @IsString()
    tags: string;

    //@IsNumber()
    category: any;

    

}
