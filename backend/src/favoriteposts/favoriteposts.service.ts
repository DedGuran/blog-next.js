import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';

import { UpdateFavoritepostDto } from './dto/update-favoritepost.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FavoritepostsEntity } from './entities/favoritepost.entity';
import { CreateFavoritepostsDto } from './dto/create-favoritepost.dto';

@Injectable()
export class FavoritepostsService {
  constructor(
    @InjectRepository(FavoritepostsEntity)
    private repository: Repository<FavoritepostsEntity>
  ) { }
  create(dto: CreateFavoritepostsDto) {
    console.log('t',dto.userId,dto.postId)
    return this.repository.save(dto)
 
  }

  findAll() {
    
    return this.repository.find({
      order: {}
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} favoritepost`;
  }

  update(id: number, updateFavoritepostDto: UpdateFavoritepostDto) {
    return `This action updates a #${id} favoritepost`;
  }

  async remove(id: number) {
    const find = await this.repository.findOne({ where: { id: +id } })
  
    if (!find) {
      throw new NotFoundException("Статья не найдена!")
    }
    // if (find.user.id !== userId) {
    //   throw new ForbiddenException('Нет доступа к этой статье')
    // }
    
    return this.repository.delete(id);
  }

}
