import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { FavoritepostsService } from './favoriteposts.service';
import { UpdateFavoritepostDto } from './dto/update-favoritepost.dto';
import { FavoritepostsEntity } from './entities/favoritepost.entity';
import { User } from 'src/decorators/user.decorator';
import { CreateFavoritepostsDto } from './dto/create-favoritepost.dto';

@Controller('favoriteposts')
export class FavoritepostsController {
  constructor(private readonly favoritepostsService: FavoritepostsService) {}

  @Post()
  async addToFavorites( @Body() createFavoritepostDto: CreateFavoritepostsDto) {
    return this.favoritepostsService.create(createFavoritepostDto);
  }
  @Get()
  findAll() {
    return this.favoritepostsService.findAll();
  }


  @Delete(':id')
  async removeFromFavorites(@Param('id') id: number) {
    return this.favoritepostsService.remove(id);
  }
}
