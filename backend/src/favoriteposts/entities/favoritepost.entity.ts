import { PostEntity } from "src/post/entities/post.entity";
import { Column, Entity,ManyToOne,PrimaryGeneratedColumn } from "typeorm";

@Entity("favoriteposts")
export class FavoritepostsEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    userId: string;

    @ManyToOne(() => PostEntity, { eager: true })
    post: PostEntity; // Assuming Post is the target entity
}
