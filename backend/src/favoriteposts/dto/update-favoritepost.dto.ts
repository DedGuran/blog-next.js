import { PartialType } from '@nestjs/mapped-types';
import { CreateFavoritepostsDto } from './create-favoritepost.dto';

export class UpdateFavoritepostDto extends PartialType(CreateFavoritepostsDto) {}
