import { Module } from '@nestjs/common';
import { FavoritepostsService } from './favoriteposts.service';
import { FavoritepostsController } from './favoriteposts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FavoritepostsEntity } from './entities/favoritepost.entity';

@Module({
  imports: [TypeOrmModule.forFeature([FavoritepostsEntity])],
  controllers: [FavoritepostsController],
  providers: [FavoritepostsService]
})
export class FavoritepostsModule {}
