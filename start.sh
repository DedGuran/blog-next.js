bashCopy code
#!/bin/bash

# Change to the backend directory
cd /var/public_html/blog-next.js/backend

# Start the Nest.js server using PM2
pm2 start npm --name "nest-server" -- run start:prod

# Change to the frontend directory
cd ../frontend

# Start the Next.js development server using PM2
pm2 start npm --name "next-server" -- run dev
